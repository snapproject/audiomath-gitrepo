#!/usr/bin/env python
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$

r"""
If you're a user and you're seeing this file, it's presumably because
you checked out a bleeding-edge copy of audiomath in the form of a
git repository.  In that case, the best way to use this file is
to install the included working copy of the Python package,
`./python/audiomath`, as an "editable" package::

    python -m pip install -e .

(where it is assumed that the current working directory `.` is the
directory that contains this `setup.py` file).


The other usage of this file is to package audiomath up for release,
which only the audiomath project maintainers will need to do. So, if
that's not you, you can stop reading now.  If it is you, then here's
how it works:

Your Python distro will need the following prerequisites installed::

    python -m pip install wheel    # only necessary for older Python distros
    python -m pip install twine

As always, make sure `python` invokes the correct Python! That also applies
during the following release procedure:

1. Verify/ensure that the version number in `python/audiomath/MASTER_META` is
   new, i.e. it exceeds the largest version number available on PyPI at
   https://pypi.org/project/audiomath/#files

2. Commit everything to the `master` branch.

3. Update the content of the `release` branch::

       git checkout release
       git merge master -m "release X.Y.Z"
       git checkout master
       # do not post-bump the version numbers yet: setup.py will need them
   
4. Make and upload the release::

       python setup.py bdist_wheel --universal
       python -m twine upload dist/*
       rm -rf build dist python/audiomath.egg-info
       
       # To test, using a distro that has a previous pip-installed version:
       python -m pip install --upgrade audiomath --no-deps
   
5. As soon as the upload is done, maybe *already* bump the version number
   in `MASTER_META`, and commit those changes (but only to the `master`
   branch). The two advantages to that are (i) that any unreleased changes you
   now make will be associated with a version that is distinct from all pippable
   releases, and (ii) that you won't have to take any action in step 1 next time
   around.

6. Push everything to the server repo::

       git push --all --follow-tags

7. After a short while, check https://audiomath.readthedocs.io (for the release
   version) and https://audiomath.readthedocs.io/en/latest (for the master
   branch). Refresh both pages in the browser and verify the new version
   numbers in each.
"""
import os
import sys
import inspect
import setuptools

package_dir = 'python'

# import audiomath itself - but make sure it's the to-be-installed version and not some legacy version hanging over
try: __file__
except: __file__ = inspect.getfile( inspect.currentframe() )
sys.path.insert( 0, os.path.join( os.path.dirname( __file__ ), package_dir ) )
import audiomath
sys.path.pop( 0 )

meta = audiomath.__meta__
meta[ 'indent' ] = audiomath.Meta.Indenter( meta ) # allows things like long_description='...\n* {indent[  citation]}\n...'.format( **meta )

# https://packaging.python.org/tutorials/distributing-packages/#setup-args
setup_args = dict(
	name = 'audiomath',
	description = 'A package for recording, reading, manipulating, playing and writing sound files',
	long_description = """\
{long_description}

See {homepage} for full documentation and installation instructions.

If you use audiomath in your work, please cite:

* {indent[  citation]} ::

      {indent[      bibtex]}

""".format( **meta ),
	long_description_content_type='text/x-rst',
	license = "GPL v3+",
	url = meta[ 'homepage' ],
	author = meta[ 'author' ],
	author_email = meta[ 'email' ],
	version = meta[ 'version' ],
	python_requires = '>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, <4',
	install_requires = [
		'numpy',
		'psutil   ; platform_system=="Windows"', # dependency of pycaw_fork for manipulating system volume on Windows
		'comtypes ; platform_system=="Windows"', # dependency of pycaw_fork for manipulating system volume on Windows
	],
	extras_require = { 
		'plotting':    [ "matplotlib" ],
		'stretching':  [ "librosa" ],
		'audioread':   [ "audioread" ],
	},
	package_dir = {
		'' : package_dir,  # NB: can't be absolute, can't go upwards
	},
	#packages = [],
	#package_data = {},
	#data_files = [], # NB: when pip-installed, items listed here get installed *outside* the package directory
	**audiomath.Manifest( 'setup' )
)

if __name__ == '__main__':
	if len( sys.argv ) < 2:
		subcmd = 'bdist_wheel --universal'
		sys.argv += subcmd.split()
		print( 'Assuming subcommand: ' + subcmd )
		
	for i in range( 3 ):
		try:
			setuptools.setup( **setup_args )
		except:
			# This is where we attempt to accommodate older versions of pip/setuptools :
			if   i == 0: setup_args[ 'install_requires' ] = [ req.split()[ 0 ] for req in setup_args[ 'install_requires' ] ] # platform_requires was introduced in PEP508
			elif i == 1: setup_args.pop( 'python_requires') # python_requires was introduced in PEP440
			else: raise
		else:
			break
