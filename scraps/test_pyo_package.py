import os, sys, time

import audiomath as am
s = am.TestSound( '12' )

import pyo

# working from:
# - https://www.psychopy.org/_modules/psychopy/sound/backend_pyo.html
# - http://ajaxsoundstudio.com/pyodoc/

server = pyo.Server( sr=s.fs, nchnls=s.nChannels )
server.boot()
server.start()
table = pyo.DataTable( size=s.nSamples, chnls=s.nChannels, init=s.y.T.tolist() )
snd = pyo.TableRead( table, freq=table.getRate(), loop=False, mul=1.0 ) # `mul` is volume
snd.out()
while snd.isPlaying(): print( 'yes, this is asynchronous' ); time.sleep( 0.050 )
