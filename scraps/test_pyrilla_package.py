"""
git clone --recursive https://github.com/swistakm/pyrilla
cd pyrilla
cmake gorilla-audio/build
cmake --build . --config Release
python setup.py build
python -m pip install .
"""
import os, sys, time

import audiomath as am
s = am.TestSound( '12' )

import pyrilla.core as pc
snd = pc.Sound( s.filename.encode( 'utf8' ), "wav".encode( 'utf8' ) )

v = pc.Voice( snd )

v.play()
while v.playing:
	pc.update() # this is required, in order to keep audio data flowing
	# so you can do other stuff here, in the main loop
	# but you cannot push the pc.update() loop into a subsidiary thread...