void * NewPlayer( fs )
int GetNumberOfChannels( void * player );
int SetNumberOfChannels( void * player, int nChannels );
int GetNumberOfSamples(  void * player );
int SetNumberOfSamples(  void * player, int nSamples );
double GetSamplingFrequency( void * player );
double SetSamplingFrequency( void * player, double samplingFrequencyHz );
double GetSpeed( void * player );
double SetSpeed( void * player, double speed );
int GetNextSampleIndex( void * player );
int SetNextSampleIndex( void * player, int sampleIndex );


SetDataPointer( void * player, void * )