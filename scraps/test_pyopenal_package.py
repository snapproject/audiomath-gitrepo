import os
import sys
import time

import audiomath as am
s = am.TestSound( '12' )

import openal # python -m pip install pyopenal  
s.Write( 'tmp.wav' )
snd = openal.oalOpen( 'tmp.wav' )   # trying to load the full 8-channel wav file leads to  ArgumentError: argument 2: <class 'TypeError'>: wrong type
snd.play()
while snd.get_state() == openal.AL_PLAYING: print( 'yes, this is asynchronous' ); time.sleep( 0.050 )
os.remove( 'tmp.wav' )
