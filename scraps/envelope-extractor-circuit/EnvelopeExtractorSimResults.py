#!/usr/bin/env python
"""
This file contains a log of (and plotting routines for)
Bernd Battes' simulation results, from the simulations he
designed and ran to predict the decay time of the impulse
response of his envelope-extractor circuit.

The corresponding simulator file is EnvelopeExtractor_LTspice.asc
and it runs in the LTspice simulator software  (free from
https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html )

The simulation's dependent measures are:
   1. fall time (time taken to decay after offset of +/- 2V
      input signal) using screen cursor 
   2. output amplitude (+/- how many mV)

loadResistance for our OptoBox is ~ 100 kOhm
R_in = 50  (assumed output impedance of source)
Lowpass: resistance 24 kOhm, capacitance 68 nF.
"""

import numpy, numpy as np, matplotlib.pyplot as plt

# input:
loadResistance_kOhm = numpy.array( [ 1,        2,        4,      8,      16,     32,    64,     128,   256,   512,   1024,  2048  ], dtype=float )
# simulation results:
fallTime_sec        = numpy.array( [ 0.000469, 0.000826, 0.0018, 0.0035, 0.0071, 0.012, 0.0259, 0.039, 0.065, 0.106, 0.168, 0.245 ], dtype=float )
outputVoltage_mV    = numpy.array( [ 59,       99,       148,    215,    321,    486,   704,    953,   1191,  1396,  1551,  1655  ], dtype=float )


plt.clf()
plt.subplot( 121 ); plt.cla()
plt.plot( np.log10( loadResistance_kOhm ), 1000 * fallTime_sec, '*-' )
#plt.plot( np.log10( loadResistance_kOhm ), np.log10( fallTime_sec ), '*-' ); plt.gca().set( yticklabels=[ '%.3g' % y for y in 10 ** plt.gca().get_yticks() * 1000 ] )
plt.ylabel( 'fall time (ms)' )
xt = loadResistance_kOhm ; plt.gca().set( xticks=np.log10( xt ), xticklabels=[ '%g' % x for x in xt ] )
plt.xlabel( 'load resistance (kOhm)' )
plt.gca().grid( True )


plt.subplot( 122 ); plt.cla()
plt.plot( np.log10( loadResistance_kOhm ), np.log10( outputVoltage_mV ), '*-' )
plt.gca().grid( True )
xt = loadResistance_kOhm ; plt.gca().set( xticks=np.log10( xt ), xticklabels=[ '%g' % x for x in xt ] )
plt.xlabel( 'load resistance (kOhm)' )
plt.ylabel( 'log$_{10}$(output in millivolts)' )

for ax in plt.gcf().axes:
	ax.tick_params( axis='both', which='major', labelsize=7 )


plt.draw()
plt.show()
