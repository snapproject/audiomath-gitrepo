The Envelope Extractor circuit was designed by Jeremy
Hill and Bernd Battes, and implemented by Bernd, around
2009.  It was used to enable an EEG amplifier with a
sampling frequency of 500Hz to record (in its auxiliary
channels) the envelope of the sound output by a PCI
surround-sound card installed in a desktop computer.

The analog output of the card was about +/- 1--2 Volts,
on top of a large (~ 1V) DC offset. This offset motivated
the inclusion of the "DC pulldown" resistor. Apart from
that, an RC circuit high-pass-filters the signal above
10Hz, then a Schottky diode half-wave-rectifies the
result (thereby capturing energy at *all* frequencies
above 10Hz), and a second RC circuit smooths the resulting
envelope, capturing amplitude fluctuations up to around
100Hz.

SAFETY NOTICE: If you plug the output of this circuit into
an amplifier socket that is *normally* intended for
sensors that make electrical contact with a human subject,
then you are connecting the computer's audio output and
ground to the *wrong* side of the amplifier's optical
isolation barrier and thereby circumventing its safety
features. Do not do this. Instead, in this case, you must
put additional optical isolation circuitry between the
envelope extractor circuit and the amplifier. If you are
unsure how to do this, or unsure whether this applies,
consult an expert.

DISCLAIMER: This design is distributed in the hope that
it will be useful and provide insight, but WITHOUT ANY
WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Use caution, and consult a qualified electronics
expert, while implementing and before deploying it.

The .cddx and .svg files were created by using the online
editor at https://www.circuit-diagram.org/editor/
The .cddx file is actually a zip archive.

The .asc file was created by Bernd using the free LTspice
simulator software from
https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html

The .py file contains, and can be used to plot, the results
of simulations performed by Bernd using LTspice.
