import sys
import time

import sys
import time

import audiomath as am
s = am.TestSound( '12' )

import pygame

import pygame.mixer
import pygame.sndarray
if not pygame.mixer.get_init(): pygame.mixer.init( frequency=int( s.fs ), channels=s.nChannels )

if 0:
	pygame.mixer.music.load( s.filename )
	pygame.mixer.music.play( loops=1, start=0.0) # `start` is measured in seconds
	while pygame.mixer.music.get_busy(): print( 'yes, this is asynchronous' ); time.sleep( 0.050 )
else:
	#snd = pygame.mixer.Sound( s.filename ) # tries to superimpose all 8 channels
	snd = pygame.sndarray.make_sound( ( s.y * 32767 ).astype( 'int16', order='C' ) )
	snd.play() # it is asynchronous, but how do we detect whether it's playing?
	
	# Two sounds can be mixed simultaneously and independently:
	time.sleep( 0.25 )
	snd2 = pygame.sndarray.make_sound( ( s.y * 32767 ).astype( 'int16', order='C' ) )
	snd2.play() # it is asynchronous, but how do we detect whether it's playing?


	# pygame.mixer also has a class called Channel, which sounds like it *might* be a
	# more-explicit way of mixing simultaneous sounds while retaining more control...
	# (presumably each "Channel" would be multi-channel, despite the name??)
