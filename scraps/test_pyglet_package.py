import os, sys, time

import audiomath as am
s = am.TestSound( '12' )

#os.environ['PYGLET_SHADOW_WINDOW'] = '0'
#import pyglet; pyglet.options[ 'audio' ] = ( 'openal', 'directsound', 'pulse', 'silent' )
import pyglet.media # here macOS crashes with AttributeError: dlsym(0x7fa016fd6e10, av_version_info): symbol not found (2020-03-02, pyglet 1.5.0, macOS 10.13.6, python 3.7.3)

s.Write( 'tmp.wav' ) # TODO: don't know how to create a sound from an array in memory, so let's do it from file (and since multichannel sounds are not supported we'll have to create a temporary stereo-only file) 
snd = pyglet.media.load( 'tmp.wav', streaming=False )
try: os.remove( s.filename )
except: pass
	
#src.play()  # this would be no different from creating a Player as follows (both ways are asynchronous) but it would give you less control, e.g. no .pause() method
p = pyglet.media.Player()
p.queue( snd )
p.play() # NB: here Windows cannot handle > 2 channels---would fail with OSError: [WinError -2147024809] The parameter is incorrect

#while p._playing: time.sleep( 0.050 ); print( 'yes, this is asynchronous' )
# well, it *is* asynchronous, but even with p.loop=False and snd.duration=1.0, what we seem to get is the first 0.5 sec played on infinite repeat (2020-03-02, pyglet 1.5.0, python 3.7.3 and 3.8.0)...

