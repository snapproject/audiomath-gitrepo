import os, sys, time

import audiomath as am
s = am.TestSound( '12' )

import soundcard # Last investigated 2020-02
sp = soundcard.default_speaker()
with sp.player( s.fs, s.nChannels ) as pp:
	"""
	NB: the `with` statement is necessary, or `pp.__enter__()` must
	otherwise be called: without it, `pp.play()` will raise an
	exception internally, complaining of missing attributes.
	"""
	if sys.platform.lower() in [ 'darwin' ]:  # also linux? not researched
		pp.play( s.y, wait=False )
		while pp._queue: print( 'yes, this is asynchronous' ); time.sleep( 0.050 )
	else:
		"""
		NB: the `wait` argument is available on macOS but not
		Windows---without it, `pp.play(s.y)` is synchronous.
		Could perhaps attempt to shove `pp.play` into a
		threading.Thread...
		
		Also: `pp._queue` appears to be a macOS-specific attribute.
		"""
		pp.play( s.y )
# NB: after `pp.__exit__()`, `pp` CANNOT now be re-used


# TODO: is there a way of stopping the sound prematurely,
# or changing its volume while playing, even on macOS?
