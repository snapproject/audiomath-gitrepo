# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$

import collections

if False: # switch to this once this is done and you want to include it as a submodule of audiomath
	from . import Base;         from .Base         import Sound, ACROSS_SAMPLES
	from . import IO;           from .IO           import Load_AVBin
	from . import Functional;   from .Functional   import Synth
	from . import Dependencies; from .Dependencies import numpy
else:
	from audiomath.Base       import Sound, ACROSS_SAMPLES
	from audiomath.IO         import Load_AVBin
	from audiomath.Functional import Synth
	import numpy

class StreamingSource( Synth ):
	"""
	TODO: this doesn't work yet. It's an attempt to use the
	existing machinery to play a sound streamed from disk
	through AVbin. Its performance seems logically consistent,
	but it crackles.
	"""
	def __init__( self, filename ):
		self.filename = filename
		self.src = Load_AVBin().AVbinSource( filename )
		fmt = self.src.audio_format
		self.duration = self.src.duration  # properties of superclass
		self.nChannels = fmt.channels      # properties of superclass
		self.fs = float( fmt.sample_rate ) # properties of superclass
		self.__firstSampleCached = 0
		self.__nextSampleToCache = 0
		self.__snd = Sound( fs=self.fs, nChannels=self.nChannels, bits=fmt.sample_size )
		self.__dtype = numpy.dtype( self.__snd.dtype_in_memory )
		self.__bytesPerSample = self.nChannels * numpy.dtype( self.__snd.dtype_encoded ).itemsize
		self.__chunks = collections.deque()
		self.__concatenated = None
		self.__subscripts = [ slice( None ), slice( None ) ]
		self._ReadChunk() # fills in self.__samplesPerChunk
	dtype = property( lambda self: self.__dtype ) # overshadows non-property attribute of superclass
	def _ReadChunk( self ):
		dataPacket = self.src.get_audio_data( 4096 )
		nSamplesRead = self.__samplesPerChunk = dataPacket.length // self.__bytesPerSample
		self.__nextSampleToCache += nSamplesRead
		self.__chunks.append( self.__snd.str2dat( dataPacket.data, nSamplesRead, self.nChannels ) )
		self.__concatenated = None	
	def func( self, fs, samples, channels ):  # overshadows method/attribute of superclass
		# `channels` argument will be ignored
		nSamplesRequested = samples.size
		firstSampleRequested = samples.flat[ 0 ]
		# TODO: assert that `samples` contains contiguous integers
		chunks = self.__chunks
		if not self.__firstSampleCached <= firstSampleRequested < self.__nextSampleToCache:
			targetSample = ( firstSampleRequested // self.__samplesPerChunk ) * self.__samplesPerChunk
			self.__firstSampleCached = self.__nextSampleToCache = targetSample
			chunks.clear()
			self.src.seek( targetSample / fs )
		while firstSampleRequested + nSamplesRequested > self.__nextSampleToCache:
			#print( 'sample %04d: reading chunk' % ( firstSampleRequested, ) )
			self._ReadChunk()
		while True:
			samplesInFirstChunk = chunks[ 0 ].shape[ ACROSS_SAMPLES ]
			if firstSampleRequested < self.__firstSampleCached + samplesInFirstChunk: break
			#print( 'sample %04d: firstCached=%d, secondCached=%d, nextToCache=%d, discarding chunk' % ( firstSampleRequested, self.__firstSampleCached, self.__firstSampleCached + samplesInFirstChunk, self.__nextSampleToCache ) )
			self.__firstSampleCached += samplesInFirstChunk
			chunks.popleft()
			self.__concatenated = None
		conc = self.__concatenated
		if conc is None:
			#print( 'sample %04d: concatenating %d chunks' % ( firstSampleRequested, len( chunks ) ) )
			conc = self.__concatenated = chunks[ 0 ] if len( chunks ) == 1 else numpy.concatenate( chunks, axis=ACROSS_SAMPLES )
		offset = firstSampleRequested - self.__firstSampleCached
		subs = self.__subscripts
		subs[ ACROSS_SAMPLES ] = slice( offset, offset + nSamplesRequested )
		return conc[ tuple( subs ) ]

if __name__ == '__main__':
	import os, sys, time, audiomath, audiomath as am, numpy, numpy as np
	ss = StreamingSource( am.PackagePath( '../../tmp/AnotherOneBitesTheDust.mp3' ) )
	p = am.Player( ss, verbose=True )
	