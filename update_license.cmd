#!/usr/bin/env python
""" "
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::

where python.exe 1>NUL 2>NUL && goto :skipconfig
:: (because any python will do, for this script)

set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set PYTHONHOME=%CUSTOMPYTHONHOME%
set PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
"""

import os, re, sys, ast, inspect

def Canonicalize( *path_pieces ):
	return os.path.realpath( os.path.join( *path_pieces ) ).replace( '\\', '/' ).rstrip( '/' )

try: __file__
except NameError:
	try: frame = inspect.currentframe(); __file__ = inspect.getfile( frame )
	finally: del frame  # https://docs.python.org/3/library/inspect.html#the-interpreter-stack
THIS = Canonicalize( __file__ )
HERE = Canonicalize( os.path.dirname( THIS ) )

packageName = 'audiomath'
meta = ast.literal_eval( open( Canonicalize( HERE, 'python', packageName, 'MASTER_META' ) ).read() )
LICENSE_HEADER = meta[ 'license_template' ].format( **meta )
LICENSE_HEADER_BEGIN_TOKEN = '$BEGIN_{}_LICENSE$'.format( packageName.upper() )
LICENSE_HEADER_END_TOKEN   =   '$END_{}_LICENSE$'.format( packageName.upper() )
LICENSE_HEADER = '\n{}\n\n{}\n\n{}\n'.format( LICENSE_HEADER_BEGIN_TOKEN, LICENSE_HEADER.strip(), LICENSE_HEADER_END_TOKEN )
print( '"""' + LICENSE_HEADER + '"""' )

def Main():
	suffix = '_updated'
	for parentDir, subDirs, fileNames in os.walk( HERE ):
		if '.hg' in subDirs: subDirs.remove( '.hg' )
		if '.git' in subDirs: subDirs.remove( '.git' )
		for fileName in fileNames:
			if os.path.islink( os.path.join( parentDir, fileName ) ): continue
			filePath = Canonicalize( parentDir, fileName )
			if filePath == THIS: continue
			if filePath.endswith( suffix ): continue
			with open( filePath, 'rb' ) as inputFH:
				try: content = inputFH.read().decode( 'UTF-8' )
				except UnicodeError: continue
			if LICENSE_HEADER_BEGIN_TOKEN not in content: continue
			
			nCRLFs = content.count( '\r\n' )
			nCRs = content.count( '\r' ) - nCRLFs
			nLFs = content.count( '\n' ) - nCRLFs
			nLines = max( nCRLFs, nCRs, nLFs )
			lineEnding = '\n' if nLFs == nLines else '\r\n' if nCRLFs == nLines else '\r'
			licenseHeaderPattern = re.compile( r"""
				(\r\n|\r|\n)(?P<comment>[ \t]*\S+[ \t]+){beginToken}
				(
					( (\r\n|\r|\n)(?P=comment)[^\r\n]* )*?
					( (\r\n|\r|\n)(?P=comment){endToken} )
				)?
			""".format(
				beginToken = re.escape( LICENSE_HEADER_BEGIN_TOKEN ),
				endToken   = re.escape( LICENSE_HEADER_END_TOKEN ),
			), re.VERBOSE )
			def Replace( match ):
				return LICENSE_HEADER.rstrip().replace( '\n', lineEnding + match.group( 'comment' ) )
	
			newContent = licenseHeaderPattern.sub( Replace, lineEnding + content, count=1 )[ len( lineEnding ): ]
			if newContent != content:
				print( 'updating ' + filePath )
				oldMode = os.stat( filePath ).st_mode
				with open( filePath, 'wb' ) as outputFH:
					outputFH.write( newContent.encode( 'UTF-8' ) )
				os.chmod( filePath, oldMode )


if __name__ == '__main__':
	sys.exit( Main() )
