.. audiomath documentation master file
   This file should at least contain the root `toctree` directive.

.. image:: AudiomathLogo-OneTwo-Transparent.png
	:align: right
	:scale: 10%

.. include:: auto/Welcome.rst

.. toctree::
   :caption: Table of Contents
   :maxdepth: 2
   
   auto/Installation
   auto/Examples
   source/modules
   auto/Support
   auto/License


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
