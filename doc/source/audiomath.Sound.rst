The `Sound` class
=================

.. autoclass:: audiomath.Sound
	:members:
	:show-inheritance:
	:inherited-members:
