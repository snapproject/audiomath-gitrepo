The `audiomath` package
=======================

.. contents:: :local:

Summary
-------

.. automodule:: audiomath

Classes
-------

- `audiomath.Sound` for manipulating, visualizing and writing sounds.
- `audiomath.Synth` for functionally generating sounds on-the-fly instead of storing them as static arrays.
- `audiomath.Player` for playing sounds.
- `audiomath.Queue` for managing playlists (every `Player` automatically has one of these).
- `audiomath.Fader` and `audiomath.Delay` are examples of "dynamic" callable objects that can be assigned to `Player` properties.
- `audiomath.Recorder` for recording sounds---see also the `Record()` function.
- `audiomath.ffmpeg` for streaming audio data to file via the auxiliary third-party utility ffmpeg, from https://ffmpeg.org
- `audiomath.sox` for transforming audio data via the auxiliary third-party utility sox, from https://sox.sourceforge.net

Global functions and constants
------------------------------

.. autofunction:: audiomath.Concatenate

.. autofunction:: audiomath.MakeFall

.. autofunction:: audiomath.MakeHannWindow

.. autofunction:: audiomath.MakePlateau

.. autofunction:: audiomath.MakeRise

.. autofunction:: audiomath.QueueTest

.. autofunction:: audiomath.Record

.. autofunction:: audiomath.Stack

.. autofunction:: audiomath.TestSound

.. autofunction:: audiomath.ToneTest

