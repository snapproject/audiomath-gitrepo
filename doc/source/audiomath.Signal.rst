The `audiomath.Signal` sub-module
=================================

.. automodule:: audiomath.Signal
	:members:
	:member-order: alphabetical
	:exclude-members: fft, ifft, Hann