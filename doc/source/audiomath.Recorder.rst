The `Recorder` class
====================

.. autoclass:: audiomath.Recorder
	:members:
	:show-inheritance:
	:inherited-members:
