The `audiomath.PortAudioInterface` sub-module
=============================================

.. automodule:: audiomath.PortAudioInterface
	:members:
	:member-order: alphabetical