The `Queue` class
=================

.. autoclass:: audiomath.Queue
	:members:
	:show-inheritance:
	:inherited-members:
