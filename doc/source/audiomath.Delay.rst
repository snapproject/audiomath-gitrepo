The `Delay` class
=================

.. autoclass:: audiomath.Delay
	:members:
	:show-inheritance:
	:inherited-members:
