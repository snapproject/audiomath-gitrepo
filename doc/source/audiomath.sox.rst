The `sox` class
===============

.. autoclass:: audiomath.sox
	:members:
	:show-inheritance:
	:inherited-members:
