The `Fader` class
=================

.. autoclass:: audiomath.Fader
	:members:
	:show-inheritance:
	:inherited-members:
