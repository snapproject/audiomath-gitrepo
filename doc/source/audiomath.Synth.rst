The `Synth` class
=================

.. autoclass:: audiomath.Synth
	:members:
	:show-inheritance:
	:inherited-members:
