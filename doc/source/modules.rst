API reference
=============

.. toctree::
   :maxdepth: 4

   audiomath
   audiomath.Sound
   audiomath.Synth
   audiomath.Player
   audiomath.Queue
   audiomath.Fader
   audiomath.Delay
   audiomath.Recorder
   audiomath.ffmpeg
   audiomath.sox
   audiomath.PortAudioInterface
   audiomath.PsychToolboxInterface
   audiomath.Signal
   audiomath.SystemVolume
   