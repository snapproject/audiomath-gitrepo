The `Player` class
==================

.. autoclass:: audiomath.Player
	:members:
	:show-inheritance:
	:inherited-members:
