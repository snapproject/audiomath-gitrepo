The `ffmpeg` class
==================

.. autoclass:: audiomath.ffmpeg
	:members:
	:show-inheritance:
	:inherited-members:
