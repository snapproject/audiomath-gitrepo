#!/usr/bin/env python
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$

"""
x = MultiWindow( sourceFileName ).Scrambled();  x.Play()
"""

import os
import sys
import random
import audiomath

class MultiWindow( object ):
	def __init__( self, source, windowLengthInSeconds=0.5, riseDurationInSeconds=0.05 ):
		self.source = audiomath.Sound( source )
		self.MakeWindow( windowLengthInSeconds, riseDurationInSeconds )
	
	def MakeWindow( self, windowLengthInSeconds=None, riseDurationInSeconds=None ):
		if windowLengthInSeconds is not None or riseDurationInSeconds is not None:
			if windowLengthInSeconds is None: windowLengthInSeconds = self.window.Duration()
			if riseDurationInSeconds is None: riseDurationInSeconds = windowLengthInSeconds / 2.0
			plateauDurationInSeconds = windowLengthInSeconds * 0.5 - riseDurationInSeconds
			silentDurationInSeconds = plateauDurationInSeconds / 2.0
			fs = self.source.fs
			self.window = silentDurationInSeconds % audiomath.MakeRise( riseDurationInSeconds, fs=fs, hann=True ) % audiomath.MakePlateau( plateauDurationInSeconds, fs=fs ) % audiomath.MakeFall( riseDurationInSeconds, fs=fs, hann=True ) % silentDurationInSeconds
	
	def NumberOfSegments( self ):
		windowStrideInSamples = self.window.NumberOfSamples() / 2.0
		return int( self.source.NumberOfSamples() / windowStrideInSamples ) + 1
		
	def Segment( self, segmentNumber, windowed=True, numberOfSegments=None ):
		if numberOfSegments is None: numberOfSegments = self.NumberOfSegments()
		if segmentNumber < 0: segmentNumber += numberOfSegments
		if segmentNumber >= numberOfSegments: return None
		isShort = segmentNumber in ( 0, numberOfSegments - 1 )
		segmentLengthInSamples = self.window.NumberOfSamples()
		windowStrideInSamples = segmentLengthInSamples / 2.0
		if isShort: segmentLengthInSamples /= 2.0
		startOffsetInSamples = max( 0, int( ( segmentNumber - 1 ) * windowStrideInSamples ) )
		segment = self.source.y[ startOffsetInSamples : int( startOffsetInSamples + segmentLengthInSamples ), : ]
		segmentLengthInSamples = segment.shape[ 0 ]
		if not segmentLengthInSamples: return None
		if windowed:
			winStart = 0 if segmentNumber else int( windowStrideInSamples )
			segment = segment * self.window.y[ winStart : winStart + segmentLengthInSamples, : ]
		rms = segment.std( axis=0 ).mean()
		segment = audiomath.Sound( segment, fs=self.source.fs )
		segment.startOffsetInSamples = startOffsetInSamples
		segment.stopOffsetInSamples = startOffsetInSamples + segmentLengthInSamples 
		segment.amp = rms
		segment.isShort = isShort
		return segment

	def Check( self, segments, context=0.05 ): # NB: reconstructions only produce the correct result with *windowed* segments
		segments = list( segments )
		segments.sort( key=lambda x: x.startOffsetInSamples )
		startInSeconds = [ self.source.SamplesToSeconds( segment.startOffsetInSamples ) for segment in segments ]
		stopInSeconds  = [ self.source.SamplesToSeconds( segment.stopOffsetInSamples  ) for segment in segments ]
		overallStartInSeconds = max( 0, min( startInSeconds ) - context )
		comparison = self.source[ max( 0, min( startInSeconds ) - context ) : min( self.source.Duration(), max( stopInSeconds ) + context ) ]
		reconstruction = comparison *  0
		for segment, shift in zip( segments, startInSeconds ):
			piece = ( shift - overallStartInSeconds ) % segment
			comparison &= piece
			reconstruction += piece
		comparison = reconstruction & comparison
		comparison = Reconstruct( segments ) & comparison
		comparison.AutoScale()
		comparison.Plot()
		
	
	@property
	def raw_segments( self ):
		n = self.NumberOfSegments()
		for i in range( n ): yield self.Segment( i, windowed=False, numberOfSegments=n )

	@property
	def windowed_segments( self ):
		n = self.NumberOfSegments()
		for i in range( n ): yield self.Segment( i, windowed=True,  numberOfSegments=n )
	
	def Scrambled( self, windowLengthInSeconds=None, riseDurationInSeconds=None, levels=20 ):		
		self.MakeWindow( windowLengthInSeconds, riseDurationInSeconds )
		segments = list( self.windowed_segments )
		segments.sort( key=lambda x: x.amp )
		stride = max( 2, int( round( len( segments ) / float( levels ) ) ) )
		#print( stride )
		shuffled = []
		while segments:
			bin, segments = segments[ :stride ], segments[ stride: ]
			shuffled += ShuffleSegmentOffsets( bin )
		return Reconstruct( shuffled )

def ShuffleSegmentOffsets( segments ):
	segments = list( segments )
	short_segments = [ segment for segment in segments if segment.isShort ]
	long_segments  = [ segment for segment in segments if not segment.isShort ]
	offsets = [ ( segment.startOffsetInSamples, segment.stopOffsetInSamples ) for segment in long_segments ]
	random.shuffle( long_segments )
	for offset, segment in zip( offsets, long_segments ):
		segment.startOffsetInSamples, segment.stopOffsetInSamples = offset
	return long_segments + short_segments

def Reconstruct( segments ):
	segments = list( segments )
	length = max( segment.stopOffsetInSamples for segment in segments )
	template = segments[ 0 ]
	reconstruction = audiomath.Silence( length, template.NumberOfChannels(), dtype=template )
	for segment in segments:
		reconstruction[ segment.startOffsetInSamples : segment.stopOffsetInSamples, : ] += segment.y
	return audiomath.Sound( reconstruction, fs=template.fs )

if __name__ == '__main__':
	args = sys.argv[ 1: ]
	mw = MultiWindow( args[ 0 ] )
	normal = mw.source
	scrambled = mw.Scrambled()
	name, xtn = os.path.splitext( os.path.basename( normal.filename ) )
	scrambled.Write( name + '_scrambled.wav' )
	print( scrambled )
	( normal & scrambled ).Plot( maxDuration=30 )
	p = audiomath.Player( scrambled )
	p.Play()
	audiomath.LoadPyplot().show()
	del p