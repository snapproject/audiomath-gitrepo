#!/usr/bin/env python

# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$

import os
import sys
import glob
import time
import inspect

import audiomath


try: frame = inspect.currentframe(); HERE = os.path.dirname( inspect.getabsfile( frame ) )
finally: del frame

patterns = sys.argv[ 1: ]
if not patterns: patterns = [ os.path.join( HERE, '..', '..', 'test', 'mac_alex_yes.wav' ) ]
filenames = [ match for pattern in patterns for match in sorted( glob.glob( os.path.realpath( pattern ) ) ) ]
for filename in filenames:
	s = audiomath.Sound( filename )
	print( s )
	p = audiomath.Player( s )
	p.Play( wait=True )
if not filenames: print( 'no files matched %r' % patterns )
if filenames: print(sys.getrefcount(p)-1)
