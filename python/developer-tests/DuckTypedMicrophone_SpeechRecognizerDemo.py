# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$

import audiomath; audiomath.RequireAudiomathVersion( '1.12.0' )
import speech_recognition  # NB: python -m pip install SpeechRecognition

class DuckTypedMicrophone( speech_recognition.AudioSource ): # descent from AudioSource is required purely to pass an assertion in Recognizer.listen()
	def __init__( self, device=None, chunkSeconds=1024/44100.0 ):  # 1024 samples at 44100 Hz is about 23 ms
		self.recorder = None
		self.device = device
		self.chunkSeconds = chunkSeconds
	def __enter__( self ):
		self.nSamplesRead = 0
		self.recorder = audiomath.Recorder( audiomath.Sound( 5, nChannels=1 ), loop=True, device=self.device )
		# Attributes required by Recognizer.listen():
		self.CHUNK = audiomath.SecondsToSamples( self.chunkSeconds, self.recorder.fs, int )
		self.SAMPLE_RATE = int( self.recorder.fs )
		self.SAMPLE_WIDTH = self.recorder.sound.nbytes
		return self
	def __exit__( self, *blx ):
		self.recorder.Stop()
		self.recorder = None
	def read( self, nSamples ):
		sampleArray = self.recorder.ReadSamples( self.nSamplesRead, nSamples )
		self.nSamplesRead += nSamples
		return self.recorder.sound.dat2str( sampleArray )
	@property
	def stream( self ): # attribute must be present to pass an assertion in Recognizer.listen(), and its value must have a .read() method
		return self if self.recorder else None

if __name__ == '__main__':
	import speech_recognition as sr
	r = sr.Recognizer()
	with DuckTypedMicrophone() as source:
		print('\nSay something to the %s...' % source.__class__.__name__)
		audio = r.listen(source)
	print('Got it.')
	print('\nUnderstood: "%s"\n' % r.recognize_google(audio))

	if 0: # plot and/or play back captured audio
		s = audiomath.Sound(audio.get_wav_data(), fs=audio.sample_rate, nChannels=1)
		s.Play()
		s.Plot()
