#!/usr/bin/env python
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
"""
Defines the `Looper` class, for looped time-stretched playback
of audio segments.

Requires the third-party package `librosa`.
"""

import audiomath, audiomath as am
import audiomath.StretchAndShift  # python -m pip install librosa

def _ResolveTime( snd, t, isEnd=False ):
	duration = snd.duration
	if t is None: t = duration if isEnd else 0.0
	if isinstance( t, str ): t = am.Base.DecodeTimecodeString( t )
	if t < 0.0: t += duration
	return float( t )

NOW = 'now'
CURSOR = False

class Looper( object ):
	"""
	A `Looper` object lets you load a sound, narrow it down to a
	segment of interest, play that segment on a continuous loop,
	and independently adjust its speed and pitch. Its purpose is
	to help you work out how to play part of a piece of music
	(e.g. a guitar solo) from the recording.
	
	Tip: while playing, you can assign `self.start = 'now'` or
	`self.end = 'now'` to set the start or end point to the
	current instantaneous playback position.
	"""
	def __init__( self, source, start=None, end=None, speed=1.0, pitch=0, verbose=False ):
		"""
		The only required argument is `source`: a sound filename or
		`Sound` instance.
		
		All constructor arguments initialize instance properties
		of the corresponding name---see documentation for the
		individual properties. Properties may be adjusted after
		construction, even during playback (causing the corresponding
		changes to happen on-the-fly).
		
		NB: do not specify a `speed` other than 1.0, or a `pitch`
		other than 0 unless you have already adjusted (or are
		simultaneously adjusting, in a constructor call or `.Set()`
		method call) the `start` and `end` properties so that they
		are a few seconds apart: time-stretching or pitch-shifting
		an entire song, or even a minutes' worth of audio, will take
		a long time. It's much better to narrow down your loop
		segment *first*, and *then* stretch and/or pitch-shift it.
		"""
		self.__source = None
		self.__speed = 1.0
		self.__pitch = 0.0
		self.__start = None
		self.__end   = None
		self.__player = am.Player( source, loop=True )
		self.__artist = None
		self.verbose = False
		self.Set( source=source, start=start, end=end, speed=speed, pitch=pitch, verbose=verbose )
	
	def Set( self, **kwargs ):
		"""
		Set multiple property values simultaneously, and only
		reprocess the loop segment's audio data once.
		"""
		artist = kwargs.pop( 'artist', self.__artist )
		oldSource, oldSpeed, oldPitch, oldStart, oldEnd = self.__source, self.__speed, self.__pitch, self.__start, self.__end
		newSource = kwargs.pop( 'source', self.__source )
		newSpeed  = kwargs.pop( 'speed',  self.__speed  )
		newPitch  = kwargs.pop( 'pitch',  self.__pitch  )		
		newStart  = kwargs.pop( 'start',  self.__start  )
		newEnd    = kwargs.pop( 'end',    self.__end    )
		for k, v in kwargs.items():
			if hasattr( self, k ): setattr( self, k, v )
			else: raise AttributeError( "%s class has no property %r" % ( self.__class__.__name__, k ) )
		if not isinstance( newSource, am.Sound ): newSource = am.Sound( newSource )
		oldStart = _ResolveTime( newSource, oldStart )
		oldEnd   = _ResolveTime( newSource, oldEnd, isEnd=True )
		absolutePosition = self.__player.head * oldSpeed + oldStart
		if newStart in [ 'now', 'NOW' ]: newStart = absolutePosition
		if newEnd   in [ 'now', 'NOW' ]: newEnd   = absolutePosition
		newStart = _ResolveTime( newSource, newStart )
		if not oldEnd and not newEnd: newEnd = None
		newEnd   = _ResolveTime( newSource, newEnd, isEnd=True )
		absolutePosition = min( max( absolutePosition, newStart ), newEnd )
		if newSource is not oldSource or newSpeed != oldSpeed or newPitch != oldPitch or newStart != oldStart or newEnd != oldEnd:
			if self.verbose: print( '%r is calculating [%g:%g] x %g + %g' % ( self, newStart, newEnd, newSpeed, newPitch ) ); t0 = am.Seconds()
			segment = newSource[ newStart : newEnd ]
			playerSpeed = 2.0 ** ( newPitch / 12.0 )
			relativeSpeed = newSpeed / playerSpeed
			if abs( relativeSpeed - 1.0 ) > 1e-6: segment.TimeStretch( speed=relativeSpeed )
			self.__player.sound = segment
			self.__player.speed = playerSpeed
			self.__player.head = ( absolutePosition - newStart ) / newSpeed
			if self.verbose: print( '%r finished calculating (time elapsed: %g sec)' % ( self, am.Seconds() - t0 ) )
		self.__source, self.__speed, self.__pitch, self.__start, self.__end = newSource, newSpeed, newPitch, newStart, newEnd
		if artist:
			if newSource is not oldSource: self.Plot()
			else: artist.SetSpan( [ self.__start, self.__end ] )
		
	def _FilterKwargs( self, d ):
		filtered = { k : d.pop( k ) for k in list( d ) if hasattr( self, k ) } 
		self.Set( **filtered )
		return d
	
	def Play( self, **kwargs ):  self.__player.Play(  **self._FilterKwargs( kwargs ) )
	def Pause( self, **kwargs ): self.__player.Pause( **self._FilterKwargs( kwargs ) )
	def Stop( self, **kwargs ):  self.__player.Stop(  **self._FilterKwargs( kwargs ) )
	
	def Rewind( self ): self.__player.Seek( 0 )
	def Seek( self, t, relative=False ): self.__player.Seek( t, relative=relative )
	def Skip( self, t ): self.__player.Seek( t, relative=True )
	
	def StartHere( self ): self.start = NOW
	def StopHere( self  ): self.end   = NOW
	EndHere = StopHere
	
	@property
	def source( self ):
		"""
		This is the master `Sound` object from which the loop segment
		is computed. You can also assign a filename here, in which case
		the sound is automatically loaded.
		"""
		return self.__source
	@source.setter
	def source( self, value ): self.Set( source=value )
	@property
	def speed( self ):
		"""
		This floating-point value specifies the speed at which to
		play the loop segment (using time-stretching, independent
		of any change in pitch).

		NB: do not specify a `speed` other than 1.0, or a `pitch`
		other than 0 unless you have already adjusted (or are
		simultaneously adjusting, in a constructor call or `.Set()`
		method call) the `start` and `end` properties so that they
		are a few seconds apart: time-stretching or pitch-shifting
		an entire song, or even a minutes' worth of audio, will take
		a long time. It's much better to narrow down your loop
		segment *first*, and *then* stretch and/or pitch-shift it.
		"""
		return self.__speed
	@speed.setter
	def speed( self, value ): self.Set( speed=value )
	@property
	def pitch( self ):
		"""
		This integer or floating-point value specifies the optional
		pitch shift, as a positive or negative number of semitones,
		to apply to your loop segment (independent of speed).
		
		NB: do not specify a `speed` other than 1.0, or a `pitch`
		other than 0 unless you have already adjusted (or are
		simultaneously adjusting, in a constructor call or `.Set()`
		method call) the `start` and `end` properties so that they
		are a few seconds apart: time-stretching or pitch-shifting
		an entire song, or even a minutes' worth of audio, will take
		a long time. It's much better to narrow down your loop
		segment *first*, and *then* stretch and/or pitch-shift it.
		"""
		return self.__pitch
	@pitch.setter
	def pitch( self, value ): self.Set( pitch=value )
	@property
	def start( self ):
		"""
		This is a floating-point timecode in seconds. You can also
		assign it as a timecode string (`MM:SS[.FFF]`). This property
		specifies where in the `source` to start playing on each loop.
		If you assign the string `'now'` during active playback, the
		start point gets set to the current instantaneous playback
		position.
		"""
		return self.__start
	@start.setter
	def start( self, value ): self.Set( start=value )
	@property
	def end( self ):
		"""
		This is a floating-point timecode in seconds. You can also
		assign it as a timecode string (`MM:SS[.FFF]`). This property
		specifies where in the `source` to stop playing and loop back
		to the `start`.  If you assign the string `'now'` during active
		playback, the end point gets set to the current instantaneous
		playback position.
		"""
		return self.__end
	@end.setter
	def end( self, value ): self.Set( end=value )

	def Plot( self ):
		"""
		TODO: this method is not finished
		
			- needs animated playhead cursor that doesn't disturb playback (search for `cursor` in code)
			- needs keyboard shortcut for pitch-shifting
			- needs buttons
			- doesn't quit cleanly
		"""
		self.source.Plot( finish=False )
		
		import sys, time, threading
		from audiomath._scraps.Artists import StickySpanSelector
		plt = am.Graphics.LoadPyplot()
		from matplotlib.animation import FuncAnimation

		ax = plt.gca()
		ax.yaxis.set_ticklabels( [] )
		ax.yaxis.set_tick_params( width=0 )
		span = [ self.start, self.end ] if self.source.duration else None
		self.__pending = None

		def UpdateTitle():		
			artist = self.__artist
			if artist:
				title = artist.axes.get_title().split( '\n' )
				if len( title ) < 2: title.append( '' )
				title[ -1 ] = '(speed=%g, pitch=%+g)' % ( self.__speed, self.__pitch )
				artist.axes.set_title( '\n'.join( title ) )
		def OnSelect( start, end ):
			self.__pending = am.Seconds(), dict( start=start, end=end )
			UpdateTitle()
		ss = self.__artist = StickySpanSelector(
			ax,
			onselect = OnSelect,
			initial = span,
			granularity = 0.010,
			fmt = am.Base.MakeTimecodeString,
			text_verticalalignment = 'top',
			text_y = 0.5,
		)
		UpdateTitle()
		def ProcessQueue():
			t0 = am.Seconds()
			if self.verbose: print( '%r: starting ProcessQueue()' % self )
			while ss.IsAlive():
				if self.__pending:
					timeAdded, kwargs = self.__pending
					self.__pending = None
					if self.verbose: print( '\n%r: %11.6f -> %11.6f : %r' % ( self, timeAdded - t0, am.Seconds() - t0, kwargs ) ); sys.stdout.flush()
					self.Set( artist=None, **kwargs )
				else:
					time.sleep( 0.001 )
			if self.verbose: print( '%r: terminating ProcessQueue()' % self )
			# TODO: termination is no longer happening---.IsAlive() is no longer reliable for some reason
		threading.Thread( target=ProcessQueue ).start()
		cursor = None
		if CURSOR: # TODO: cursor animation causes sound to break up slightly
			cursor, = plt.plot( [ 0 ], [ 0.5 ], 'k|', transform=ax.get_xaxis_transform(), rasterized=True, markersize=50 )
			#cursor, = plt.plot( [ 0, 0 ], [ 0, 1 ], color='k', transform=ax.get_xaxis_transform(), rasterized=True )
			animated = [ cursor, self.__artist.rect, self.__artist.mintext, self.__artist.maxtext ]
			def UpdateCursor( frame ):
				cursor.set_xdata( [ self.__player.head * self.speed  + self.start ] )
				return animated
		if cursor:
			self.__anim = FuncAnimation( ax.figure, UpdateCursor, blit=True, cache_frame_data=True )
			
		@ss.AddKeyPressResponder
		def KeyPress( ss, key, mods ):
			if key == 'up':   return Zoom( 0.5, mods )
			if key == 'down': return Zoom( 2.0, mods )
			if key == '[' and self.__player.playing: self.start = NOW; ss._lastMoved = 0; return
			if key == ']' and self.__player.playing: self.end   = NOW; ss._lastMoved = 1; return
			if key in [ '1', '2', '3', '4', '5', '6', '7', '8', '9' ]: self.speed = 1 / float( key ); return
			if key in [ ' ' ]: self.__player.playing = not self.__player.playing; return
			print( repr( mods ), repr( key ) )
			return True
		def Zoom( factor, mods ):
			span = ss.GetSpan()
			lastMoved = span[ ss._lastMoved ]
			xmin, xmax = 0, self.source.duration
			if xmax <= xmin: xmax = xmin + 1.0
			ax = ss.axes
			oldLower, oldUpper = oldLimits = ax.get_xlim()
			oldHalfWidth = ( oldUpper - oldLower ) / 2.0
			newHalfWidth = oldHalfWidth * factor
			def NewRange( centered, *focalPoint ):
				focalPoint = sum( focalPoint ) / float( len( focalPoint ) )
				if centered: lims = [ focalPoint - oldHalfWidth * factor, focalPoint + oldHalfWidth * factor ]
				else: lims = [ ( x - focalPoint ) * factor + focalPoint for x in oldLimits ]
				return [ min( max( x, xmin ), xmax ) for x in lims ]
			def InRange( range_, *x ):
				return all( min( range_ ) < xi < max( range_ ) for xi in x )
			centeredOnSpanCenter = NewRange( True, *span )
			containingLastMoved = NewRange( False, lastMoved )
			centeredOnLastMoved = NewRange( True, lastMoved )
			centeredOnOldCenter = NewRange( True, *oldLimits )
			if 0: pass
			elif InRange( containingLastMoved, lastMoved ) and InRange( containingLastMoved, *span ) and InRange( oldLimits, lastMoved ) and not 'shift' in mods: xlim = containingLastMoved
			elif InRange( centeredOnLastMoved, lastMoved ) and InRange( oldLimits, lastMoved ) and not 'shift' in mods: xlim = centeredOnLastMoved
			elif InRange( containingLastMoved, lastMoved ) and InRange( oldLimits, lastMoved ) and not 'shift' in mods: xlim = containingLastMoved
			elif InRange( centeredOnSpanCenter, *span ) and InRange( oldLimits, *span ): xlim = centeredOnSpanCenter
			else: xlim = centeredOnOldCenter
			ax.set( xlim=xlim )
			plt.draw()
		am.Graphics.FinishFigure( zoom=False, pan=False )

if __name__ == '__main__':
	import os, sys, argparse
	parser = argparse.ArgumentParser( description="TODO" )
	parser.add_argument( 'FILENAME', action='store', nargs='?' )
	parser.add_argument( "-s", "--start",  metavar='TIMECODE',  action='store', default=None, help='segment start time, in seconds or as MM:SS.FFF timecode' )
	parser.add_argument( "-e", "--end",    metavar='TIMECODE',  action='store', default=None, help='segment end time, in seconds or as MM:SS.FFF timecode' )
	parser.add_argument( "-k", "--cursor", action='store_true', default=False, help='TODO' )
	opts = parser.parse_args( args=None ) # args=sys.argv[ 1: ]
	if not opts.FILENAME:
		testfile = am.PackagePath( '../../tmp/AnotherOneBitesTheDust.mp3' ) # won't exist on most people's systems
		if os.path.isfile( testfile ):
			opts.FILENAME = testfile
			if opts.start is None and opts.end is None:
				opts.start, opts.end = 60, 64
	if not os.path.isfile( opts.FILENAME ): raise SystemExit( 'must supply a valid filename' )
	x = Looper( opts.FILENAME, start=opts.start, end=opts.end, verbose=True )
	print( """\

space            toggle play/pause

enter            toggle which boundary is being moved
left / right     move boundary (shift for larger increments, alt for smaller)
[                move start position (left boundary) to current play position
]                move end position (right boundary) to current play position

up / down        zoom into/away from most-recently-moved boundary

1                play at original speed
2                play at 1/2 original speed
3                play at 1/3 original speed
etc...

""")
	if opts.cursor: CURSOR = True
	plt = am.Graphics.LoadPyplot(); plt.close( 'all' ); x.Plot() 
