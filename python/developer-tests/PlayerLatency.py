#!/usr/bin/env python
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
r"""
This is designed to be used with a microcontroller that
communicates over the serial port. It was developed using
a Teensy 3.1 board. The Teensy needs to have the
corresponding sketch `PlayerLatency-TeensySketch.ino`
loaded, needs to be connected to the computer by USB
(using a cable that transfers data and power, not a
cheap power-only cable), and needs an audio input that
you connect to the computer's headphone jack. For
example, Teensy's own "Audio Adapter Board" product
provides a 3.5mm stereo audio input jack---in any case,
you'll need to solder something onto the board, and
change the `.ino` file to ensure that the correct pin
number is being used for the audio input signal. To
flash the sketch onto the Teensy, make sure you have
the Arduino IDE (v1.8+) installed, with "teensyduino"
installed on top of it. Select the correct board model
and serial port via the menus before clicking Upload.
See the comments in `PlayerLatency-TeensySketch.ino`
for more detailed instructions.

The microcontroller measures the time between receiving a
particular serial-port command `primed=1\n` and detecting
evidence of a high-amplitude sound being played.

This Python wrapper defines a `LatencyTest` class that
prepares an appropriate `Sound`, initializes a `Player`,
and gets ready both to deliver the serial-port command
and then, immediately afterward, to play the sound. It
receives the micro-controller's time measurement
('internal') but also records the entire round-trip time
including the serial-port communication overhead
('external').  The `.Test()` method then does this
repeatedly, under two conditions: with or without the
sound playing constantly on a loop---if it is playing
constantly ('looped' condition) then the 'internal'
measurement should always be close to 0.

This script can be run from the command-line: use `--help`
to see the command-line options, that allow configuration,
saving and plotting of the test. It requires `audiomath`,
`numpy` and `pyserial`, and also `matplotlib` if you want
to plot results.

In our tests on both macOS and Windows 10, the 'external'
measurement for the 'looped' condition is between 0.5ms
and 1ms (increasing to 1.5ms for a few machines that
might have slower USB) with a standard deviation of less
than 0.1 to 0.3 ms. This serial overhead measurement is,
accordingly, almost exactly the difference between the
'internal' and 'external' measurements in both conditions,
and it provides an upper bound for any unaccounted-for
audio delay---i.e. the unknown delay in receiving the
initial serial-port command and hence the delay in starting
the timer that measures audio latency. (Actually, it's
very likely that the unaccounted-for delay is *half* of
the difference, and possibly less, but it's impossible to
know for sure.)
"""

BAUD = 38400                     # must match Teensy sketch
TRIGGER_SAMPLING_RATE_HZ = 10000 # must match Teensy sketch; must divide into 1e6
PRIMING_COMMAND = 'primed=1\n'.encode( 'utf8' )
TEENSY_VARS = 'primed threshold pulse_duration_msec report_period_msec debug_sample_timing'

DIRT_SIMPLE = False # to set this to True, you also have to recompile the Teensy sketch after commenting out the line that does  #include "SerialInterface.h"
if DIRT_SIMPLE: PRIMING_COMMAND = '\n'.encode( 'utf8' )

"""
    ::
          |<----------external-------->|   # definition in the paper (what we should have done)
              |<--------external------>|   # definition in the code (what we're stuck with doing)
          port.write()
          |   player.Play() |        
          |   |   port.readline()
          |<s>|<p>|
          *   *   **********************   <= Python Interpreter
          .\  .                       /.
          . \ .<----a---->#<-d->.    / .     
          .  \                  .   /  .
          .   \                 .  /   .
          .    \                . /    .
          .     \               ./     .
          |<-u1->|<--internal-->|<-u2->|   <= Microcontroller

    *  - Python interpreter command
    #  - Physical sound output starts
    \/ - Serial-port communication to and from the microcontroller
    u1 - unknown latency to send & receive priming command
    u2 - unknown latency to send & receive reply
         u1 + u2 can be estimated as either of the following:
             `looped.send + looped.external - looped.internal`
             `unlooped.send + unlooped.external - unlooped.internal`
         which empirically comes out to TODO
    s  - time taken for write() command: 0.1 ms (MacBook Pro) up to
         0.5 ms (EZPad), On a Surface Pro 3 (Windows 10, Python 3.7.3)
         it was  0.15--0.2 ms, although it could be up to 0.5ms the
         very first time, and on the Horizon II running Ubuntu Linux
         18.04 it was 0.3 ms.  Unfortunately early versions of this
         program did not keep a permanent trial-by-trial record of s.
    p  - time taken for Play() command to return (measured on Surface
         Pro 3 with Windows 10 and Python 3.7:  0.02 ms using
         PortAudioInterface or 0.08 ms using PsychToolboxInterface).
    a  - audio latency, our main measure of interest (although note
         that the overall latency of a {software-triggered logging
         + presentation} system is s+a
    d  - detection time <= 0.1ms (mean 0.02ms but std 0.04ms) - this
         can be measured as the time between receiving the priming
         command, and detecting the presence of a sound that's already
         ongoing (that's the purpose of the "looped" condition). It
         is measured as `looped.internal` and is the same on all
         platforms and machines tested (because it's purely a function
         of the microcontroller).
    c  - time taken to query clock and return to Python interpreter
         (will be considered negligible as it's 0.001--0.005 ms)

    Using the code's definition of `external`:
    
        external = a + d + u2
                 = u1 + internal + u2 - s
    
        a = u1 + internal - s - d
        u1 + u2 = s + external - internal
       
        [Assume u1 = u2 = (s + external - internal) / 2]
        [Assume d is negligible ]
    
        a = (s + external - internal)/2 + internal - s
          = (external + internal)/2 - s/2

    Using the paper's definition of `external`:
    
        external = s + a + d + u2
                 = u1 + internal + u2
    
        a = u1 + internal - s - d       # same as code
        u1 + u2 = external - internal
       
        [Assume u1 = u2 = (external - internal) / 2]
        [Assume d is negligible ]
    
        a = (external - internal)/2 + internal - s
          = (external + internal)/2 - s

In this script, the role of the PsychToolbox pre-scheduling option
(if used) is purely to create a fixed predictable-length latency
relative to the first `Seconds()` call.  If you use the WASAPI
host API (PTB's default) with `--fs=48000` and latency class 2/3/4,
and you pre-schedule the sound---for example with `--ptb=3:50`
for a 50-millisecond delay---then you can expect the estimate
`(external+internal)/2` to come out to your specified 50ms,
plus `d + y`, i.e. about 0.2ms over. As at 20200207 this seems
to be about right on macOS, and about 1ms late on Windows 10
(running under WASAPI at 48000 Hz with latency class 3).
"""

import os
import sys
import ast
import time
import glob
import socket
import argparse
import threading

import audiomath, audiomath as am
import numpy, numpy as np
serial = audiomath.DependencyManagement.Require( 'serial:pyserial' )

from audiomath._wrap_portaudio import Bunch

class Timeout( object ):
	def __init__( self, port, timeout ): self.port, self.timeout = port, timeout
	def __enter__( self ): self.previousTimeout, self.port.timeout = self.port.timeout, self.timeout
	def __exit__( self, *blx ):	self.port.timeout = self.previousTimeout

class LatencyTest( object ):
	
	@classmethod
	def Load( cls, filename ):
		return cls( from_file=filename )
		
	def __init__( self, serialPortAddress=None, device=None, timeout=2, threshold=None, bufferLengthMsec=None, minLatencyMsec=None, from_file=None, soundSamplingFrequencyHz=44100, swapCalls=False, delayMsec=None, hostname=None ): 
		
		self.sound = None
		self.player = None
		self.requestedDevice = None
		self.port = None
		self.raw = None
		self.summary = None
		self.swapCalls = swapCalls
		self.delaySeconds = delayMsec / 1000.0 if delayMsec else 0.0
		self.hostname = hostname if hostname else socket.gethostname()
		
		if isinstance( bufferLengthMsec, str ):
			if bufferLengthMsec.lower() in [ '', 'none' ]: bufferLengthMsec = None
		try: bufferLengthMsec = float( bufferLengthMsec )
		except: pass
		if isinstance( bufferLengthMsec, ( int, float ) ) and bufferLengthMsec < 0: bufferLengthMsec = None 
			
		if isinstance( minLatencyMsec, str ):
			if minLatencyMsec.lower() in [ '', 'none' ]: minLatencyMsec = None
		try: minLatencyMsec = float( minLatencyMsec )
		except: pass
		if isinstance( minLatencyMsec, ( int, float ) ) and minLatencyMsec < 0: minLatencyMsec = None 
			
		if threshold is not None and threshold <= 0: threshold = None
		
		if not soundSamplingFrequencyHz: soundSamplingFrequencyHz = 44100
		self.settings = Bunch(
			threshold = threshold,
			bufferLengthMsec = bufferLengthMsec,
			minLatencyMsec = minLatencyMsec,
			delayMsec = delayMsec,
			soundSamplingFrequencyHz = float( soundSamplingFrequencyHz ),
		)
		
		if from_file:
			self.summary = Bunch._read( from_file )
			self.settings = Bunch( self.summary.settings )
			
		if device is not None or not from_file:
			self.settings.soundSamplingFrequencyHz = float( soundSamplingFrequencyHz )
			self.triggerSamplesPerSoundPeriod = 3.0
			self.soundFundamentalFrequencyHz = TRIGGER_SAMPLING_RATE_HZ / float( self.triggerSamplesPerSoundPeriod )

			soundSamplesPerPeriod = am.SecondsToSamples( 1.0 / self.soundFundamentalFrequencyHz, self.settings.soundSamplingFrequencyHz, rounding=None )
			samples = numpy.arange( 1000.0 ) * soundSamplesPerPeriod
			error = numpy.abs( samples - numpy.round( samples ) )
			seconds = am.SamplesToSeconds( samples, self.settings.soundSamplingFrequencyHz )
			acceptable = ( seconds >= 0.05 ) & ( error < 1e-10 )
			samples = numpy.round( samples[ acceptable ] ).astype( int ).min()
			seconds = am.SamplesToSeconds( samples, self.settings.soundSamplingFrequencyHz )

			waveform = am.Signal.SquareWave
			self.sound = am.Signal.GenerateWaveform(
				container = am.Sound( fs=self.settings.soundSamplingFrequencyHz ),
				duration_msec = seconds * 1000,
				freq_hz = self.soundFundamentalFrequencyHz,
				maxharm = int( 0.5 * self.settings.soundSamplingFrequencyHz / self.soundFundamentalFrequencyHz ),
				waveform = waveform,
			).AutoScale().Set( label='%s%gHz' % ( waveform.__name__, self.soundFundamentalFrequencyHz ) )
			self.sound *= [ 1, 1 ]
			
			if device is not None:
				self.requestedDevice = am.FindDevice( device, mode='o' )
				device = self.requestedDevice[ 'index' ]
			self.player = am.Player( self.sound, device=device, bufferLengthMsec=bufferLengthMsec, minLatencyMsec=minLatencyMsec, fs=self.settings.soundSamplingFrequencyHz )
			print( self.player )
			self.settings.bufferLengthMsec = self.player.bufferLengthMsec
			self.settings.minLatencyMsec = self.player.minLatencyMsec
			rd = self.requestedDevice
			try: gd = self.player.stream.outputDevice
			except: gd = None
			if rd and gd and rd[ 'index' ] != gd[ 'index' ]:
				self.CleanUp()
				raise( RuntimeError( 'failed to connect to requested device\nrequested device %2d = %s -> %s\n      got device %2d = %s -> %s' % ( rd.index, rd.hostApi.name, rd.name, gd.index, gd.hostApi.name, gd.name ) ) )
			if gd:
				self.deviceDescription = 'device %2d: %s -> %s' % ( gd.index, gd.hostApi.name, gd.name )
				print( 'connected to %s' % self.deviceDescription )
			if hasattr( self.player, 'latencyClass' ):
				self.settings.latencyClass = self.player.latencyClass
			
		if serialPortAddress:
			self.port = serial.Serial( serialPortAddress, baudrate=BAUD, timeout=timeout )
			if not DIRT_SIMPLE:
				th = { 'threshold' : threshold } if threshold else {}
				with Timeout( self.port, 0.5 ): self.Send( primed=0, report_period_msec=0, debug_sample_timing=0, pulse_duration_msec=20, **th )
				self.settings.threshold = self.threshold		
	def Send( self, query=None, **kwargs ):
		port = self.port
		record = {}; before = am.Seconds()
		record[ 'command' ] = command = ';'.join( ( [ query ] if query else [] ) + [ '%s=%r' % ( k, v ) for k, v in kwargs.items() ] )
		port.write( ( command.strip() + '\n' ).encode( 'utf8' ) )
		port.flush()
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'port.write' ] = elapsed * 1000.0
		line = port.readline()
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'port.readline' ] = elapsed * 1000.0
		line = line.decode( 'utf8' ).strip()
		try: result = ast.literal_eval( line )
		except: record[ 'result' ] = line
		else:
			if isinstance( result, dict ): record.update( result )
			else: record[ 'result' ] = result
		return record
	
	def Go( self, **kwargs ): # write() before Play()
		port = self.port
		player = self.player
		
		record = {}; before = am.Seconds()
		port.write( PRIMING_COMMAND )
		port.flush()
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'port.write' ] = elapsed * 1000.0
		if self.delaySeconds: kwargs.setdefault( 'when', before + self.delaySeconds )
		player.Play( **kwargs )
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'player.Play' ] = elapsed * 1000.0
		line = port.readline()
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'port.readline' ] = elapsed * 1000.0
		record[ 'external' ] = record[ 'player.Play' ] + record[ 'port.readline' ]
		line = line.decode( 'utf8' ).strip()
		try: result = ast.literal_eval( line )
		except: record[ 'result' ] = line
		else: record.update( result )
		return record

	def Go_swapped( self, **kwargs ): # Play() before write()
		port = self.port
		player = self.player
		record = {}; before = am.Seconds()
		if self.delaySeconds: kwargs.setdefault( 'when', before + self.delaySeconds )
		player.Play( **kwargs )
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'player.Play' ] = elapsed * 1000.0
		port.write( PRIMING_COMMAND )
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'port.write' ] = elapsed * 1000.0
		line = port.readline()
		after = am.Seconds(); elapsed, before = after - before,  after
		record[ 'port.readline' ] = elapsed * 1000.0
		record[ 'external' ] = record[ 'player.Play' ] + record[ 'port.write' ] + record[ 'port.readline' ]
		line = line.decode( 'utf8' ).strip()
		try: result = ast.literal_eval( line )
		except: record[ 'result' ] = line
		else: record.update( result )
		return record
	
	def Test( self, n=100, sleepRange=( 0.25, 0.5 ), nLooped=None ):
		t0 = am.Seconds()
		from audiomath.SystemVolume import MAX_VOLUME
		if not DIRT_SIMPLE: self.settings.threshold = self.threshold # queries over serial port
		Go = self.Go_swapped if self.swapCalls else self.Go
		if nLooped is None: nLooped = n
		def progress( x ): sys.stdout.write( x + '\r' ); sys.stdout.flush()
		with MAX_VOLUME:
		
			self.player.Play( loop=True )
			time.sleep( 0.250 )
			looped = []
			for i in range( nLooped ):
				progress( '%3d of %d looped' % ( i + 1, nLooped ) )
				sleepTime = numpy.random.uniform( min( sleepRange ), max( sleepRange ) )
				time.sleep( sleepTime )
				looped.append( Go() )
			print( '' )
			self.player.Pause( loop=False )
			time.sleep( 0.250 )
			unlooped = []
			for i in range( n ):
				progress( '%3d of %d unlooped' % ( i + 1, n ) )
				sleepTime = numpy.random.uniform( min( sleepRange ), max( sleepRange ) )
				time.sleep( sleepTime )
				unlooped.append( Go() )
			print( '' )
				
			self.raw = dict( looped=looped, unlooped=unlooped )
			self.summary = self.PackageResults( self.Stats() )
			self.elapsed = am.Seconds() - t0
			return self
		
	def PackageResults( self, results ):
		device = self.player.stream.outputDevice
		computer = Bunch( hostname=self.hostname )
		computer.update( am.GetVersions() )
		
		return Bunch(
			computer = computer,
			datestamp = time.strftime( '%Y%m%d-%H%M%S' ),
			device = device,
			settings = self.settings,
			results = Bunch._convert( results ),
		)
			
	def Stats( self, x=None ):
		if x is None: x = self.raw
		if isinstance( x, dict ): return { k : self.Stats( v ) for k, v in x.items() }
		send     = numpy.array( [ xi.get( 'port.write',  numpy.nan ) for xi in x ] )
		play     = numpy.array( [ xi.get( 'player.Play', numpy.nan ) for xi in x ] )
		internal = numpy.array( [ xi.get( 'internal',    numpy.nan ) for xi in x ] )
		external = numpy.array( [ xi.get( 'external',    numpy.nan ) for xi in x ] )
		external[ numpy.isnan( internal ) ] = numpy.nan
		writeable = lambda x: [ ( None if numpy.isnan( xi ) else xi ) for xi in x ] # suitable for writing to a text file
		d = {}
		def AddMeasurement( d=None, **kwargs ):
			for name, values in kwargs.items():
				d[ name ] = writeable( values )
				d[ name + '_mean' ] = numpy.nanmean( values )
				d[ name + '_std' ] = numpy.nanstd( values )
				d[ name + '_n' ] = sum( ~numpy.isnan( values ) )
			return d
		AddMeasurement( d, send=send )
		AddMeasurement( d, play=play )
		AddMeasurement( d, external=external )
		AddMeasurement( d, internal=internal )
		return d
	
	@property
	def title( self ):
		def specified( x ):
			if x is None: return False
			if isinstance( x, str ): return x.lower() not in [ 'none', '' ]
			return True
		def num2str( x ):
			return '%g' % x if isinstance( x, ( int, float ) ) else str( x )
		s = '%s-%s-d%d' % ( self.summary.datestamp, self.summary.computer.hostname.replace( ' ', '' ), self.summary.device.index )
		if specified( self.settings.bufferLengthMsec ): s += '-b' + num2str( self.settings.bufferLengthMsec )
		if specified( self.settings.minLatencyMsec   ): s += '-l' + num2str( self.settings.minLatencyMsec   )
		if self.settings.get( 'latencyClass', None ) is not None:
			s += '--ptb=%d' % self.settings.latencyClass
			if self.settings.get( 'delayMsec', None ):
				s += '+%g' % self.settings.delayMsec
		if self.settings.get( 'soundSamplingFrequencyHz', 44100 ) != 44100:
			s += '--fs=%g' % self.settings.soundSamplingFrequencyHz
		return s
		
	def Plot( self, x=None, hold=False, figure=None, **kwargs ):
		plt = am.Graphics.LoadPyplot()
		if figure: plt.figure( figure ); plt.draw()
		if not hold: plt.cla()
		if x is None: x = 'unlooped.external unlooped.internal looped.external looped.internal'
		if isinstance( x, str ): x = x.split()
		if not isinstance( x, ( list, tuple ) ) or not len( x ) or not isinstance( x[ 0 ], str ): x = [ x ]
		label = kwargs.pop( 'label', None )
		for xi in x:
			eachLabel = None
			if isinstance( xi, str ):
				eachLabel, xi = xi, self.summary.results
				for part in eachLabel.split( '.' ):
					xi = xi[ part ]
				xi = numpy.array( xi, dtype=float )
				eachLabel += ' (%.2f ms $\\pm$ %.2f, N=%d)' % ( numpy.nanmean( xi ), numpy.nanstd( xi ), sum( ~numpy.isnan( xi ) ) )
			plt.plot( xi, 'x-', label=( eachLabel if eachLabel else label ), **kwargs )
		lo = 1000.0 * self.summary.device.defaultLowOutputLatency
		hi = 1000.0 * self.summary.device.defaultHighOutputLatency
		plt.plot( [ 0, 1, None, 1, 0 ], [ lo, lo, None, hi, hi ], 'k--', transform=plt.gca().get_yaxis_transform() )
		plt.legend()
		plt.grid( True )
		plt.title( '%s\n%s -> %s' % ( self.title, self.summary.device.hostApi.name, self.summary.device.name ) )
		return plt.gca()
	
	def Save( self, filename=None ):
		if not filename: filename = self.title + '-' + self.summary.device.hostApi.name.replace( ' ', '' ).replace( '-', '' ) + '.txt'
		filename = os.path.realpath( filename )
		print( 'Saving results to %s' % filename )
		self.summary._write( filename )
		
	def CleanUp( self ):
		self.player = None
		if self.port: self.port.close()
		self.port = None
		
	@classmethod
	def TeensyProperties( cls, *names ):
		for name in names:
			def fget( self, name=name ):
				return self.Send( name )[ name ]
			def fset( self, value, name=name ):
				with Timeout( self.port, 0.1 ): self.Send( **{ name : value } )
			setattr( cls, name, property( fget=fget, fset=fset, doc="interface to the Teensy's %r variable" % name ) )

LatencyTest.TeensyProperties( *TEENSY_VARS.split() )

class Monitor( object ):
	def __init__( self, port ):
		self.port = port
		self.lock = threading.Lock()
		self.inputs = []
		self.outputs = []
		self.t0 = None
		self.__keepGoing = False
		self.Start()
	def __enter__( self ): self.Start(); return self
	def __exit__( self, *blx ): self.Stop()
	def Start( self ):
		if self.__keepGoing: return
		self.__keepGoing = True
		self.thread = threading.Thread( target=self.Receive )
		self.thread.start()
	def Stop( self ):
		if not self.__keepGoing: return
		timeout = self.port.timeout
		self.__keepGoing = False
		time.sleep( timeout * 2 )
	def Send( self, query=None, **kwargs ):
		if self.t0 is None: self.t0 = am.Seconds()
		record = {}
		record[ 'command' ] = command = ';'.join( ( [ query ] if query else [] ) + [ '%s=%r' % ( k, v ) for k, v in kwargs.items() ] )
		command = ( command.strip() + '\n' ).encode( 'utf8' )
		with self.lock, Timeout( self.port, 0.1 ):
			t = am.Seconds() - self.t0
			self.port.write( command )
		record[ 't' ] = t
		self.inputs.append( record )
	def Receive( self ):
		with Timeout( self.port, 0.1 ):
			if self.t0 is None: self.t0 = am.Seconds()
			t0 = self.t0
			while self.__keepGoing:
				with self.lock:
					line = self.port.readline()
					t = am.Seconds() - t0
				if not line:
					time.sleep( 0.001 )
					continue
				line = line.decode( 'utf8' )
				try: record = ast.literal_eval( line )
				except: record = { 'line' : line }
				record[ 't' ] = t
				if self.inputs: record[ 'delta' ] = t - self.inputs[ -1 ][ 't' ]
				self.outputs.append( record )
				print( record )
				
	@classmethod
	def TeensyProperties( cls, *names ):
		for name in names:
			def fget( self, name=name ):
				nBefore = len( self.outputs )
				self.Send( name )
				while True:
					if len( self.outputs ) <= nBefore: continue
					d = self.outputs[ -1 ]
					if name in d: return d[ name ] 
					time.sleep( 0.001 )
			def fset( self, value, name=name ):
				self.Send( **{ name : value } )
			setattr( cls, name, property( fget=fget, fset=fset, doc="interface to the Teensy's %r variable" % name ) )

Monitor.TeensyProperties( *TEENSY_VARS.split() )

def Shell( **kwargs ):
	import os, sys, time, IPython, numpy, numpy as np, audiomath, audiomath as am
	locals().update( kwargs)
	del sys.argv[ 1: ]
	print( '' )
	IPython.start_ipython( user_ns=locals() )

def Main( args=None, prog=None ):
	if isinstance( prog, ( tuple, list ) ): prog = ' '.join( prog )
	parser = argparse.ArgumentParser( prog=prog, description=__doc__ )
	parser.add_argument( "-a", "--apis", "--host-apis", action='store_true', default=False, help='print a list of host APIs' )
	parser.add_argument(       "--devices", "--list-devices", action='store_true', default=False, help='print a list of devices' )
	parser.add_argument(       "--headphones", action='store_true', default=False, help='print a list of devices with the word "headphones" in the name' )
	parser.add_argument( "-d", "--device",  metavar='DEVICE_INDEX',  action='store', default=None, help='specify which device should be used for playback' )
	parser.add_argument( "-n", "--repetitions",  metavar='N',  action='store', default='4', help='number of repetitions of each test' )
	parser.add_argument( "-p", "--port",  metavar='PORT_ADDRESS',  action='store', default=None, type=str, help='port address' )
	parser.add_argument( "-f", "--figure", "--plot",  metavar='FIGURE_NUMBER',  action='store', default=0, type=int, help='figure number for plotting' )
	parser.add_argument( "-t", "--threshold", metavar='THRESHOLD',  action='store', default=0, type=float, help='threshold parameter for the Teensy sketch' )
	parser.add_argument( "-m", "--monitor", action='store_true', default=False, help='open an embedded prompt giving access to a serial port monitor instance `m` (the test instance `t` will be run when the prompt closes)' )
	parser.add_argument( "-b", "--buffer", metavar='MSEC',  action='store', default=None, help="buffer length to request, in milliseconds, when opening a Stream (you can also use the third field of the --config option). This may be `None` or `'auto'`, which means choose audiomath's own latency-vs-underflow balance setting,  or 'default' which means use the PortAudio library default buffer setting." )
	parser.add_argument( "-l", "--minlatency", metavar='MSEC',  action='store', default=None, help="setting for the 'suggested latency' (you can also use the second field of the --config option)" )
	parser.add_argument( "-r", "--read",  metavar='FILENAME',  action='store', default=None, help='load results from the specified file for plotting, instead of performing the test (this may also be a glob pattern or a colon-delimited explicit list of filenames)' )
	parser.add_argument( "-w", "--write",  action='store_true', default=None, help='whether to save results (defaults to True iff repetitions > 10)' )
	parser.add_argument(       "--ptb", "--psychtoolbox", metavar='LATENCY_CLASS[:DELAY_MSEC]',  action='store', default=None, help='if omitted, use the default back-end. If supplied, use the "PsychToolboxInterface" back-end, with the specified `latencyClass`. Use `-3` or `psychopy` as the latency class to use the `psychopy.sound` wrapper around `psychtoolbox`. Optionally append a delay in milliseconds (affixed with a colon) to use PTB pre-scheduling---latency should then be exactly equal to the specified delay.' )
	parser.add_argument(       "--fs", metavar='SAMPLING_FREQ_HZ',  action='store', default=None, type=float, help='sampling frequency for the Stream (you can also use the first field of the --config option)' )
	parser.add_argument(       "--swap-calls", action='store_true', default=False, help='if set, call `player.Play()` before `port.write()`' )
	parser.add_argument( "-x", "--detailed", action='store_true', default=False, help='if set, print more-detailed report' )
	parser.add_argument(       "--notes", "--asio4all", metavar='NOTES_ON_SETTINGS', action='store', default='', help='optional additional settings to store in the `notes` field of the settings (previously called the `ASIOALLBuffering` field)' )
	parser.add_argument( "-k", "--computer", metavar='COMPUTER_NAME', action='store', default='', help='optionally override the name of the computer/hardware configuration in the filename and report' )
	parser.add_argument( "-c", "--config", metavar='SAMPLE_RATE[:MIN_LATENCY_MSEC[:BUFFER_LENGTH_MSEC]]',  action='store', default='', help='set sampling frequency, minimum latency (msec) and/or buffer size (msec) for the test stimuli (--fs, -b and -l override these, if supplied). You can also specify buffer length in samples instead of milliseconds, by putting the value in brackets.' )
	opts = parser.parse_args( args=args )
	
	t = None
	doTest = True
	delayMsec = None
	
	if opts.ptb:
		latencyClass, delayMsec = ( opts.ptb.replace( '+', ':' ) + ':' ).split( ':', 1 )
		delayMsec = float( delayMsec.strip( ':' ) ) if delayMsec else None
		if latencyClass.lower() in [ 'psychopy', '-3' ]:
			am.BackEnd.Load( 'PsychoPyInterface' )
			am.LATENCY_CLASS.DEFAULT = -3
		else:
			am.BackEnd.Load( 'PsychToolboxInterface' )
			if latencyClass == '': latencyClass = 'DEFAULT'
			try: latencyClass = int( latencyClass )
			except: latencyClass = getattr( am.LATENCY_CLASS, latencyClass.upper() )
			am.LATENCY_CLASS.DEFAULT = latencyClass
	
	if opts.config:
		parameters = opts.config.replace( ':', '+' ).split( '+' )
		fs = minLatencyMsec = bufferLengthMsec = None
		if parameters: fs = parameters.pop( 0 )
		if parameters: minLatencyMsec = parameters.pop( 0 )
		if parameters: bufferLengthMsec = parameters.pop( 0 )
		fs = 44100 if fs.lower() == 'none' or not fs else float( fs )
		if not opts.fs: opts.fs = fs
		if not opts.minlatency: opts.minlatency = minLatencyMsec
		if not opts.buffer: opts.buffer = bufferLengthMsec

	if opts.apis:
		print( am.GetHostApiInfo() )
		doTest = False
	if opts.devices:
		d = am.FindDevices( mode='o' )
		d.sort( key=lambda d: d[ 'index' ] )
		print( d )
		print( ' ' )
		doTest = False
	if opts.headphones:
		d = am.FindDevices( 'headphones', mode='o' )
		d.sort( key=lambda d: d[ 'index' ] )
		print( d )
		print( ' ' )
		doTest = False
	
	if opts.read:
		firstFigureNumber = opts.figure if opts.figure else 1
		filenames = [ filename for pattern in opts.read.split( ':' ) for filename in sorted( glob.glob( os.path.realpath( os.path.expanduser( pattern ) ) ) ) ]
		axes = []
		for figureNumber, filename in enumerate( filenames, firstFigureNumber ):
			# beware of assigning old measurements to `t` at this point - console reporting will crash if measurements are missing. 
			axes.append( LatencyTest.Load( filename ).Plot( figure=figureNumber ) )
			am.Graphics.FinishFigure( maximize=True, wait=False )
		ylim = [ 0, max( max( ax.get_ylim() ) for ax in axes ) ]
		for ax in axes: ax.set_ylim( ylim )
		am.Graphics.FinishFigure()
		doTest = False
		
	if doTest:
		if not opts.port:
			opts.port = os.environ.get( 'TeensySerialPort', '' )	
		if not opts.port:
			if sys.platform.lower().startswith( 'win' ): opts.port = 'COM4'
			elif sys.platform.lower().startswith( 'darwin' ): opts.port = '/dev/cu.usbmodem314001'
		
		t = LatencyTest(
			serialPortAddress = opts.port,
			device = opts.device,
			bufferLengthMsec = opts.buffer,
			minLatencyMsec = opts.minlatency,
			threshold = opts.threshold,
			soundSamplingFrequencyHz = opts.fs,
			swapCalls = opts.swap_calls,
			delayMsec = delayMsec,
			hostname = opts.computer,
		)
		if opts.notes:
			t.settings.notes = opts.notes
			
		if opts.monitor:
			with Monitor( t.port ) as m: Shell( m=m, t=t )
			with Timeout( t.port, 0.5 ): t.Send( primed=0, report_period_msec=0, debug_sample_timing=0, reprime_interval_msec=0 )
		opts.repetitions = opts.repetitions.replace( ':', '+' )
		if '+' in opts.repetitions:
			nLooped, nUnlooped = [ int( x ) for x in opts.repetitions.split( '+' ) ]
		else:
			nLooped = nUnlooped = int( opts.repetitions )
		t.Test( nUnlooped, nLooped=nLooped )
		t.CleanUp()
		
			
	if t:
		#print( t.summary )
		print( '\nElapsed: %d s' % t.elapsed )
		a = lambda x: numpy.array( x, dtype=float )
		def Report( label, dp, x ):
			print( '%s %{msf}.{dp}f ms +/- %{ssf}.{dp}f (N=%d)'.format( dp=dp, msf=max( 7 if dp > 1 else 0, dp + 2 ), ssf=max( 6 if dp > 1 else 0, dp + 2 ) ) % ( label, numpy.nanmean( x ), numpy.nanstd( x ), sum( ~numpy.isnan( x ) ) ) )
		r = t.summary.results
		if opts.detailed:
			print( ' ' )
			Report( 'send (looped):          ', 3, a( r.looped.send   ) )
			Report( 'send (unlooped):        ', 3, a( r.unlooped.send ) )
			Report( 'serial (looped):        ', 3, a( r.looped.send   ) + a( r.looped.external   ) - a( r.looped.internal   ) )
			Report( 'serial (unlooped):      ', 3, a( r.unlooped.send ) + a( r.unlooped.external ) - a( r.unlooped.internal ) )
			Report( 'internal (looped) = d:  ', 3, a( r.looped.internal   ) )
			Report( 's + external (looped):  ', 3, a( r.looped.send   ) + a( r.looped.external   ) )
			Report( 'internal (unlooped):    ', 3, a( r.unlooped.internal ) )
			Report( 's + external (unlooped):', 3, a( r.unlooped.send ) + a( r.unlooped.external ) )
			print( ' ' )
		Report( 'Latency estimate: ', 1, ( a( r.unlooped.external ) + a( r.unlooped.internal ) - a( r.unlooped.send ) ) / 2.0 )
		print( ' ' )
		if opts.write is None: opts.write = ( nUnlooped > 10 )
		if opts.write: t.Save()
		if opts.figure:
			t.Plot( figure=opts.figure )
			am.Graphics.FinishFigure( maximize=True )

if __name__ == '__main__':
	Main()

"""
python PlayerLatency.py      -d Core//
python PlayerLatency.py -c:0 -d Core//

python PlayerLatency.py -n10+100 -lnone -d ASIO4ALL
python PlayerLatency.py -n10+100 -lnone -d WDM-KS//Headphones
python PlayerLatency.py -n10+100 -lnone -d WASAPI//Headphones
python PlayerLatency.py -n10+100 -lnone -d DirectSound//Headphones
python PlayerLatency.py -n10+100 -l20   -d DirectSound//Headphones
python PlayerLatency.py -n10+100 -lnone -d MME//Headphones

python PlayerLatency.py -n10+100 -lnone -d WASAPI//Headphones --ptb 4 --fs 48000
python PlayerLatency.py -n10+100 -lnone -d WASAPI//Headphones --ptb 4
python PlayerLatency.py -n10+100 -lnone -d WASAPI//Headphones --ptb 3
python PlayerLatency.py -n10+100 -lnone -d WASAPI//Headphones --ptb 2
python PlayerLatency.py -n10+100 -lnone -d WASAPI//Headphones --ptb 1 --fs 48000
python PlayerLatency.py -n10+100 -lnone -d WASAPI//Headphones --ptb 0 --fs 48000
python PlayerLatency.py -n10+100 -lnone -d MME//Headphones --ptb 4
python PlayerLatency.py -n10+100 -lnone -d MME//Headphones --ptb 3
python PlayerLatency.py -n10+100 -lnone -d MME//Headphones --ptb 2
python PlayerLatency.py -n10+100 -lnone -d MME//Headphones --ptb 1
python PlayerLatency.py -n10+100 -lnone -d MME//Headphones --ptb 0
::python PlayerLatency.py -n10+100 -lnone -d WDM-KS//Headphones --ptb 4
::python PlayerLatency.py -n10+100 -lnone -d WDM-KS//Headphones --ptb 3
::python PlayerLatency.py -n10+100 -lnone -d WDM-KS//Headphones --ptb 2
::python PlayerLatency.py -n10+100 -lnone -d WDM-KS//Headphones --ptb 1
python PlayerLatency.py -n10+100 -lnone -d WDM-KS//Headphones --ptb 0
"""
