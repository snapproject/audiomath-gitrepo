# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
"""
Snap your fingers a few times.  A click will be played in answer to
each snap, as quickly as possible. For correct automated timing
analysis, the answering click needs to be louder than the finger-snap,
so turn the system volume way up and don't snap TOO loud/close. The
time difference between between snap and click is equal to Recorder
latency plus Player latency.

Start with `-k` to use the keyboard instead: hit the enter key hard
enough that the sound will register on the recording. The time
difference between that sound and the answering click will be equal
to the Player delay plus the delay in acquiring the key-stroke.
"""

import time, itertools, argparse

import numpy

import audiomath, audiomath as am; from audiomath.SystemVolume import MAX_VOLUME

class Sensor( object ):
	def __init__( self, action=None, threshold=0.05, refractory=1.0, history=None ):
		self.lastTriggered = float( '-inf' )
		self.threshold = threshold
		self.refractory = refractory
		self.action = action
		if history == True: history = []
		if history == False: history = None
		self.history = history
	def __call__( self, src, sampleIndex, fs ):
		amp = abs( src ).max()
		if amp >= self.threshold:
			t = sampleIndex / fs
			if t - self.lastTriggered >= self.refractory:
				if self.action: self.action()
				self.lastTriggered = t
				if self.history is not None: self.history.append( t )

if __name__ == '__main__':
	
	parser = argparse.ArgumentParser( description=__doc__ )
	parser.add_argument( '-k', '--keyboard',   action='store_true', default=False, help='trigger clicks via the keyboard, instead of with other clicks' )
	parser.add_argument( '-c', '--chop',       action='store_true', default=False, help='plot chopped-up waveform, instead of continuous plot' )
	parser.add_argument( '-i', '--input',      action='store', default=None, type=int, help='input device index' )
	parser.add_argument( '-o', '--output',     action='store', default=None, type=int, help='output device index' )
	parser.add_argument( '-t', '--threshold',  action='store', default=0.05, type=float, help='threshold for Sensor (non-keyboard mode) from between 0 and 1' )
	parser.add_argument( '-r', '--refractory', action='store', default=1.0,  type=float, help='refractory period for Sensor (non-keyboard mode) in seconds' )
	parser.add_argument(       '--input-buffer',  metavar='MSEC', action='store', default=0.0,  type=float, help='input buffer length in milliseconds' )
	parser.add_argument(       '--output-buffer', metavar='MSEC', action='store', default=0.0,  type=float, help='output buffer length in milliseconds' )
	args = parser.parse_args()

	with MAX_VOLUME:
		p = am.Player( numpy.array( [ 1.0 ] * 5 + [ -1.0 ] * 5 ), device=args.output, bufferLengthMsec=args.output_buffer )
		h = am.Recorder( am.Sound( 60, nChannels=1 ), start=False, device=args.input, bufferLengthMsec=args.input_buffer ) # TODO: device arg not always obeyed....
		print( p ); print( h )
		buildup = 0.800
		t = []
		if args.keyboard:
			h.Record()
			print( 'Hit return (hard!) to trigger a click.\nTo quit, terminate an empty input line with ctrl-D' )
			time.sleep( buildup + 0.1 )
			for i in itertools.count():
				try: am.Prompt( '% 3d: ' % ( i + 1 ), catch=None )()
				except: break
				p.Play()
				t.append( h.head )
			time.sleep( p.sound.duration + 0.25 )
		else:
			h.Record()
			time.sleep( buildup + 0.1 )
			h.hook = Sensor( p.Play, threshold=args.threshold, refractory=args.refractory, history=t )
			h.WaitFor( am.Prompt( 'Snap your fingers to trigger a click.\nPress return to finish: ' ) )
			
		def Chop( z, t, buildup=buildup ):
			zz = am.Stack( z[ ti - buildup - 0.050 : ti + 0.100 ].AutoScale().Trim( threshold=0.9, buildup=buildup, tailoff=0.010 ) for ti in t )
			zz.Plot( timeShift=-buildup, timeScale=1000.0 )
			return zz
		
		z = h.Cut()
		if args.chop: Chop( z, t )
		else: z.Plot()
		am.Graphics.FinishFigure( maximize=True )