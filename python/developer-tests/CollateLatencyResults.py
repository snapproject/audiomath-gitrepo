#!/usr/bin/env python
# -*- coding: utf-8 -*-

# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
"""
TODO
"""
import os
import re
import sys
import ast
import glob
import argparse

import numpy, numpy as np

import audiomath, audiomath as am
from audiomath._wrap_portaudio import Bunch, Tabulate

generator = type( ( x for x in '' ) )

class ListOfResults( list ):
	__str__ = Tabulate
	_fieldsToTabulate = [ x for x in """
		Date-Time=datestamp
		Computer=computer.hostname
		Host API=device.hostApi.name
		fs (Hz)=settings.soundSamplingFrequencyHz
		PTB=settings.latencyClass
		Buffer (ms)=settings.bufferLengthMsec
		Min. (ms)=settings.minLatencyMsec
		Notes=settings.notes
		{measurementName}=results.{measurement}_mean:%{meanLength}.{dp}f
		(mean ± SD)=results.{measurement}_std:±%{sdLength}.{dp}f
		N=results.{measurement}_n:%3d
		Corrected=results.corrected
		#Internal=results.unlooped.internal_mean:%8.1f
		#(mean ± SD)=results.unlooped.internal_std:±%5.1f
		#External=results.unlooped.external_mean:%8.1f
		#(mean ± SD)=results.unlooped.external_std:±%5.1f
	""".strip().split( '\n' ) if not x.strip().startswith( '#' ) ]
	def __init__( self, *args, **kwargs ):
		super( ListOfResults, self ).__init__(
			record
			for arg in args
			for record in ResolveRecordGroup( arg )
			if record
		)
		correctExternal = kwargs.pop( 'correctExternal', True )
		correctForSend  = kwargs.pop( 'correctForSend',  True )
		if kwargs: raise ValueError( 'unexpected keyword arguments' )
		for record in self:
		
			delayMsec = record.settings.get( 'delayMsec', 0 )
			if delayMsec: record.settings.latencyClass = '%s:%g' % ( record.settings.latencyClass, delayMsec )

			# In tests performed before 20200207-140955 (changes committed in 417957f later that day)
			# the serial overhead (external - internal) was ~5ms (with <1ms variation between machines)
			# After that, the problem on the Teensy was fixed (with Serial.flush() calls) and the serial
			# overhead was ~1ms.   The difference 5ms -> 1ms was all on the reply side (u2, the
			# Teensy->computer latency).  Therefore the u1==u2 symmetry assumption was violated and
			# (external+internal)/2 was not a fair estimate of latency. External timing measurements
			# should have been ~4ms earlier and hence (external+internal)/2 should have been ~2ms
			# smaller. This correction is approximate, so let's mark it with a `YES` or `NO` to remind
			# us whether the correction has been applied, (or `N/A` if no correction is necessary).
			
			# The "correct for send" correction (only applied if `-s` is supplied; only possible with
			# data recorded by ac5e23b (20200301-194743) and later; marked with an 'S' in the correction
			# column) subtracts s/2 (which is the correct correction for the code, even though the paper
			# schematic says subtract s) to correct for the time-to-return of port.write().

			if record.datestamp >= '20200207-140955':
				correction = 0.0
				corrected = ' N/A'
			elif correctExternal:
				correction = 4.2
				corrected = 'YES '
			else:
				correction = 0.0
				corrected = 'NO  '
			got_send = False
			
			if 'ASIO4ALLBuffering' in record.settings and 'notes' not in record.settings:
				record.settings[ 'notes' ] = record.settings[ 'ASIO4ALLBuffering' ]
			
			for kTest, vTest in record[ 'results' ].items():
				if not isinstance( vTest, dict ): continue
				internal = vTest[ 'internal' ] = numpy.array( vTest[ 'internal' ], dtype=float )
				external = vTest[ 'external' ] = numpy.array( vTest[ 'external' ], dtype=float )
				external -= correction
				send = 0
				if 'play' in vTest:
					vTest[ 'play' ] = numpy.array( vTest[ 'play' ], dtype=float )
				if 'send' in vTest:
					vTest[ 'send' ] = numpy.array( vTest[ 'send' ], dtype=float )
					if correctForSend: send = vTest[ 'send' ]; got_send = True
				
				serial = vTest[ 'serial' ] = external - internal + send
				vTest[ 'serial_mean' ] = numpy.nanmean( serial )
				vTest[ 'serial_std' ] = numpy.nanstd( serial )
				vTest[ 'serial_n' ] = sum( ~numpy.isnan( serial ) )
				vTest[ 'serial_min' ] = numpy.nanmin( serial )
				vTest[ 'serial_max' ] = numpy.nanmax( serial )
				
				estimate = vTest[ 'estimate' ] = ( internal + external - send ) / 2
				# Subtracting `send/2` assumes `external` is measured from *after* `port.write()` but *before* `Play()`
				vTest[ 'estimate_mean' ] = numpy.nanmean( estimate )
				vTest[ 'estimate_std' ] = numpy.nanstd( estimate )
				vTest[ 'estimate_n' ] = sum( ~numpy.isnan( estimate ) )

			record[ 'results' ][ 'corrected' ] = corrected + ( '    S' if got_send else '' )
			
def GetRecord( arg ):
	if isinstance( arg, str ):
		with open( arg, 'rt' ) as fh:
			try: arg = ast.literal_eval( re.sub( r'\bnan\b', 'None', fh.read() ) )
			except: print('failed to read data from %s' % arg ); return None
	return arg if isinstance( arg, Bunch ) else Bunch._convert( arg )

def ResolveRecordGroup( arg ):
	if isinstance( arg, ( tuple, list, generator ) ):
		return [ item for subarg in arg for item in ResolveRecordGroup( subarg ) ]
	if not isinstance( arg, str ): return [ arg ]
	arg = os.path.expanduser( arg )
	arg = os.path.realpath( arg )
	if os.path.isdir( arg ): arg = os.path.join( arg, '*.txt' )
	return [ GetRecord( filename ) for filename in sorted( glob.glob( arg ) ) ]
		
def GetFields( x, *args ):
	return [ _GetField( x, name ) for arg in args for name in arg.split() ]
def _GetField( x, name ):
	for part in name.split( '.' ):
		try: x = x[ part ]
		except: return None
	return x
def Sanitize( items ):
	return [ 0 if x is None else x for x in items ]
		
if __name__ == '__main__':
	parser = argparse.ArgumentParser( description=__doc__ )
	parser.add_argument( "-u", "--unsorted", action='store_true', default=False, help='list results in the order in which the filenames are given' )
	parser.add_argument( "-c", "--chronological-order", action='store_true', default=False, help='list results in chronological order' )
	parser.add_argument( "-l", "--latency-order", action='store_true', default=False, help='list results in latency order for each computer' )
	parser.add_argument( "-n", "--no-correction", action='store_true', default=False, help='do not correct for the extra serial overhead in the "unflushed" results' )
	parser.add_argument( "-m", "--measurement", action='store', default='unlooped.estimate', help='By default, examine the `unlooped.estimate` measurement. Change this, for example, to `unlooped.internal` or `looped.external` to examine other measurements.' )
	parser.add_argument( "-d", "--dp", "--decimal-places", action='store', type=int, default=1, help='number of decimal places in millisecond measurment reporting' )
	parser.add_argument( "-s", "--send-correction", action='store_true', default=False, help='whether to subtract half the time taken for port.write()' )
	opts, argv = parser.parse_known_args()
	
	if not argv: argv.append( os.path.realpath( os.path.join( __file__, '..' ) ) )
	a = ListOfResults( argv, correctExternal=not opts.no_correction, correctForSend=opts.send_correction )
	
	measurementName = opts.measurement.replace( '.', ' ' )
	if measurementName.startswith( 'unlooped ' ): _, measurementName = measurementName.split( ' ', 1 )
	if measurementName == measurementName.lower(): measurementName = measurementName.capitalize()	
	fields = a._fieldsToTabulate
	precision = { 'dp' : opts.dp, 'sdLength' : opts.dp + 4, 'meanLength' : len( measurementName ) }
	for i, field in enumerate( fields ):
		fields[ i ] = field.format( measurement=opts.measurement, measurementName=measurementName, **precision )
					
	sortFields = 'device.hostApi.name computer.hostname settings.minLatencyMsec settings.bufferLengthMsec settings.ASIO4ALLBuffering'
	if opts.chronological_order: sortFields = 'datestamp'
	if opts.latency_order:       sortFields = 'computer.hostname results.{measurement}_mean'.format( measurement=opts.measurement )
	if not opts.unsorted:
		try:    a.sort( key=lambda x:      Sanitize( GetFields(x, sortFields ) )   )
		except: a.sort( key=lambda x: str( Sanitize( GetFields(x, sortFields ) ) ) )
			
	try: print( a )
	except: print( str( a ).replace( '±', '+/-' ) )
	#except: print( str( a ).encode( 'ascii', errors='ignore' ).decode( 'utf-8' ) )

