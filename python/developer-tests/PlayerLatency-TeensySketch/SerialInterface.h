/*
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
*/
#ifndef __SerialInterface_H__
#define __SerialInterface_H__
#include "Arduino.h"

/*
  The following example allows two global variables to be queried and/or set over the
  serial port::
  
      #include "SerialInterface.h"
    
      double gFoo = 0.0;
      String gBar = "Hello World";
    
      void setup()
      {
        Serial.begin( 9600 );
      }
    
      void loop()
      {
        if( StartCommands() )
        {
          if( ProcessVariableCommand( "foo", gFoo ) ) Serial.println( "changed foo" );
          if( ProcessVariableCommand( "bar", gBar ) ) Serial.println( "changed bar" );
          EndCommands();
        }
      }
  
  An optional third argument to `ProcessVariableCommand` can be `VARIABLE_READ_ONLY`,
  `VARIABLE_VERBOSE`, or the default value `VARIABLE_SILENT`.
  
  In this example, if you send the command `foo` over the serial port, the reply will
  be a JSON string associating the name `foo` with the current value of the corresponding
  global variable `gFoo`. 
  
  Provided the variable is not designated as `VARIABLE_READ_ONLY`, you can also set its
  value, e.g. by sending the command `foo = 2`.  (If you have designated the variable as
  `VARIABLE_VERBOSE` then this would also trigger a JSON output, as if you had queried
  `foo` immediately after setting it.)

  You can send the simple command `?` to receive a JSON output containing all the
  variables that are accessible in this manner.

  You can also have the full report delivered automatically on a repeating schedule
  by using `AutoReportVariables()` - pass a value <= 0 to disable auto-reporting, or
  a positive `periodInSeconds` to enable it; you can even allow this to be controlled
  via the serial interface, by exposing the period itself as a variable::

      #include "SerialInterface.h"
    
      double gFoo = 0.0;
      String gBar = "Hello World";
      float gReportPeriodInSeconds = 1.0;
    
      void setup()
      {
        Serial.begin( 9600 );
      }
    
      void loop()
      {
        if( StartCommands() || AutoReportVariables( gReportPeriodInSeconds ) )
        {
          if( ProcessVariableCommand( "foo", gFoo ) ) Serial.println( "changed foo" );
          if( ProcessVariableCommand( "bar", gBar ) ) Serial.println( "changed bar" );
          ProcessVariableCommand( "report_period_sec", gReportPeriodInSeconds );
          EndCommands();
        }
      }

*/

// Debugging macros:
#define REPORT( X )  { Serial.print( "{\"" #X "\" : " ); Serial.print( X );       Serial.println( "}" ); Serial.flush(); }
#define REPORTS( X ) { Serial.print( "{\"" #X "\" : " ); PrintStringLiteral( X ); Serial.println( "}" ); Serial.flush(); }


String gFullCommand = "";
int gListAllVariables = 0;

void PrintStringLiteral( const String & s, bool withQuotes=true )
{
	if( withQuotes ) Serial.print( "\"" );
	for( unsigned int i = 0; i < s.length(); i++ )
	{
		char c = s[ i ];
		if(      c == '\t' ) Serial.print( "\\t" );
		else if( c == '\r' ) Serial.print( "\\r" );
		else if( c == '\n' ) Serial.print( "\\n" );
		else if( c == '\0' ) Serial.print( "\\0" );
		else if( c ==  '"' ) Serial.print( "\\\"" );
		else if( c == '\\' ) Serial.print( "\\\\" );
		else if( isprint( c ) ) Serial.print( c );
		else
		{
			Serial.print( "\\x" );
			if( ( unsigned char )c < 16 ) Serial.print( "0" );
			Serial.print( ( unsigned char )c, HEX );
		}
	}
	if( withQuotes ) Serial.print( "\"" );
}


const char *
_ParseVariableCommand( const char *command, const char * variableName )
{
	if( !command || !*command ) return NULL;
	int lvn = strlen( variableName );
	if( strncmp( command, variableName, lvn ) != 0 ) return 0;
	command += lvn;
	while( isspace( *command ) ) command++;
	if( *command && *command++ != '=' ) return NULL;
	while( isspace( *command ) ) command++;
	return command;
}

#define _START_VARIABLE_PROCESSOR( TYPE, PRINT_FUNC ) \
	bool \
	ProcessVariableCommand( const char * variableName, TYPE & var, VariableWriteMode writeMode=VARIABLE_SILENT ) \
	{ \
		const char * command = _ParseVariableCommand( gFullCommand.c_str(), variableName ); \
		if( gListAllVariables ) \
		{ \
			Serial.print( ( gListAllVariables++ == 1 ) ? "{\"" : ", \"" ); \
			Serial.print( variableName ); \
			Serial.print( "\": " ); \
			PRINT_FUNC( var ); \
		} \
		if( !command ) return false; \
		if( !*command ) { Serial.print( "{\"" ); Serial.print( variableName ); Serial.print( "\": " ); PRINT_FUNC( var ); Serial.println( "}" ); Serial.flush(); gFullCommand = ""; return false; } \
		if( writeMode == VARIABLE_READ_ONLY ) { Serial.print( "{\"SERIAL_COMMAND_ERROR\": \"cannot change the '" ); Serial.print( variableName ); Serial.println( "' variable because it is read-only\"}" ); Serial.flush(); gFullCommand = ""; return false; } \
		char *remainder = NULL; \
		TYPE val;
		//
		// conversion of const char * command to TYPE val happens here between _START_VARIABLE_PROCESSOR and _END_VARIABLE_PROCESSOR macros
		//
#define _END_VARIABLE_PROCESSOR( TYPE, PRINT_FUNC ) \
		while( remainder && isspace( *remainder ) ) remainder++; \
		if( remainder && *remainder ) { Serial.print( "{\"SERIAL_COMMAND_ERROR\": \"failed to interpret argument as type '" #TYPE "' when setting the '" ); Serial.print( variableName ); ; Serial.println( "' variable\"}" ); Serial.flush(); gFullCommand = ""; return false; } \
		var = val; \
		if( writeMode == VARIABLE_VERBOSE ) \
		{ \
			Serial.print( "{\"" ); \
			Serial.print( variableName ); \
			Serial.print( "\": " ); \
			PRINT_FUNC( var ); \
			Serial.println( "}" ); \
			Serial.flush(); \
		} \
		gFullCommand = ""; \
		return true; \
	}

typedef enum
{
	VARIABLE_READ_ONLY = 0,
	VARIABLE_SILENT    = 1,
	VARIABLE_VERBOSE   = 2
} VariableWriteMode;

const char *
StartCommands( void )
{
	static String sPartialCommand = "";
	static bool sCommandBackslash = false;
	static char sCommandQuote = '\0';
	
	char c = '\0';
	if( !Serial.available() || ( ( c = Serial.read() ) == ';' && !sCommandQuote ) )
	{
		if( !c && !sPartialCommand.length() ) return NULL;
		gFullCommand = sPartialCommand;
		gFullCommand.trim();
		if( gFullCommand == "?" ) { gListAllVariables = 1; gFullCommand = ""; }
		sPartialCommand = "";
		sCommandBackslash = false;
		sCommandQuote = '\0';
		return gFullCommand.c_str();
	}
	bool escape = ( c == '\\' && !sCommandBackslash );
	if( sCommandBackslash )
	{
		if(      c == 'n' ) c = '\n';
		else if( c == 'r' ) c = '\r';
		else if( c == 't' ) c = '\t';
	}
	if( !escape && ( !isspace( c ) || sPartialCommand.length() ) ) sPartialCommand += c;
	if( !sCommandQuote && ( c == '\'' || c == '"' ) ) sCommandQuote = c;
	else if( sCommandQuote && c == sCommandQuote && !sCommandBackslash ) sCommandQuote = '\0';
	sCommandBackslash = escape;
	return NULL;
}

_START_VARIABLE_PROCESSOR( int, Serial.print )            val = strtol(  command, &remainder, 10 );     _END_VARIABLE_PROCESSOR( int, Serial.print )
_START_VARIABLE_PROCESSOR( unsigned int, Serial.print )   val = strtoul( command, &remainder, 10 );     _END_VARIABLE_PROCESSOR( unsigned int, Serial.print )
_START_VARIABLE_PROCESSOR( short, Serial.print )          val = strtol(  command, &remainder, 10 );     _END_VARIABLE_PROCESSOR( short, Serial.print )
_START_VARIABLE_PROCESSOR( unsigned short, Serial.print ) val = strtoul( command, &remainder, 10 );     _END_VARIABLE_PROCESSOR( unsigned short, Serial.print )
_START_VARIABLE_PROCESSOR( long, Serial.print )           val = strtol(  command, &remainder, 10 );     _END_VARIABLE_PROCESSOR( long, Serial.print )
_START_VARIABLE_PROCESSOR( unsigned long, Serial.print )  val = strtoul( command, &remainder, 10 );     _END_VARIABLE_PROCESSOR( unsigned long, Serial.print )
_START_VARIABLE_PROCESSOR( float, Serial.print )          val = ( float )strtod( command, &remainder ); _END_VARIABLE_PROCESSOR( float, Serial.print )
_START_VARIABLE_PROCESSOR( double, Serial.print )         val = strtod( command, &remainder );          _END_VARIABLE_PROCESSOR( double, Serial.print )
_START_VARIABLE_PROCESSOR( String, PrintStringLiteral )
	char quote = *command++;
	if( quote != '"' && quote != '\'' ) remainder = &quote;
	else
	{
		val = command;
		int trimmedLength;
		for( trimmedLength = val.length(); trimmedLength > 0; trimmedLength-- )
			if( !isspace( val.charAt( trimmedLength - 1 ) ) ) break;
		if( trimmedLength < ( int )val.length() ) val = val.substring( 0, trimmedLength );
		if( val.charAt( val.length() - 1 ) != quote ) remainder = &quote;
		else val = val.substring( 0, val.length() - 1 );
	}
_END_VARIABLE_PROCESSOR( String, PrintStringLiteral )

bool ProcessCommand( const char * command )
{
	if( gFullCommand != command ) return false;
	gFullCommand = "";
	return true;
}

bool
EndCommands( void )
{
	if( gListAllVariables ) { Serial.println( "}" ); Serial.flush(); gListAllVariables = 0; }
	if( gFullCommand.length() ) { Serial.println( "{\"SERIAL_COMMAND_ERROR\": \"failed to recognize command\"}" ); Serial.flush(); gFullCommand = ""; return true; }
	return false;
}

bool
AutoReportVariables( float periodInSeconds, unsigned long currentTimeInMicros )
{
	static unsigned long sTimestampOfLastAutoReport = 0;
	if( periodInSeconds > 0.0 && currentTimeInMicros - sTimestampOfLastAutoReport >= ( unsigned long )( periodInSeconds * 1e6 ) )
	{
		sTimestampOfLastAutoReport = currentTimeInMicros;
		gListAllVariables = 1;
		return true;
	}
	return false;
}

bool
AutoReportVariables( float periodInSeconds )
{
	return AutoReportVariables( periodInSeconds, micros() );
}
#endif // __SerialInterface_H__
