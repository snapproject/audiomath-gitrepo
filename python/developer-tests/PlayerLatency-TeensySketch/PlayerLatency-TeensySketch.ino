/*
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
*/

/*
This sketch implements software-triggered pulse synchronization,
hardware-triggered pulse synchronization, or both, as a way of
marking the onset time of an audio signal.  If you have both
methods enabled, then it provides a way of measuring audio
latency.

To use this sketch, you need a Teensy microcontroller, version
3.1 or later. For latency measurement or hardware-triggered pulse
generation, you need to be able to attach an audio port to the
Teensy---for example, solder a Teensy Audio Adapter onto it, or
otherwise solder the sleeve of a TRS mini-jack to the Teensy's
signal ground, and its tip and/or ring to one or two analog
input pins). You will probably want to edit the code to adjust
the definition of `DEFAULT_AUDIO_INPUT_PIN`, below, to match
whichever analog input pin you're going to use for the audio
signal.  Also connect the appropriate hardware (e.g. BNC
connectors) if you want to make use of the pulses themselves---
and accordingly adjust `DEFAULT_PRIMING_OUTPUT_PIN` for the
software-triggered pulse and `DEFAULT_TRIGGER_OUTPUT_PIN` for
the hardware-triggered pulse.  You should also verify that
`POWER_INDICATOR_LED` is set appropriately to a visible LED
for your board and doesn't clash with the other pins (the
easiest way to target the board's built-in LED is to use the
constant `LED_BUILTIN` here).

Install the Arduino IDE version 1.8 or later. Then, on top of
that installation, install Teensyduino 1.48 or later. Then when
you open up this sketch in the IDE, make sure you select the
correct board type in the Tools->Board submenu. Connect the
Teensy to the computer using a USB cable (make sure it is one
that has its data pins connected, because there are far too many
power-only micro-USB cables floating around out there). The IDE
may automatically find the appropriate serial port, or you may
need to choose it in the Tools->Port submenu. Upload the sketch
with Sketch->Upload. Bear in mind that, when the Teensy is fresh
out of the box, its very first upload may require you to press the
"reboot" button on the circuit-board manually.  When the sketch
starts up, the power LED will blink for a few seconds and then
stay on (assuming you have set `POWER_INDICATOR_LED` appropriately).

The counterpart file PlayerLatency.py is the best wrapper for
communicating with this sketch, to perform systematic audio latency
tests. However, you can send serial commands by hand for debugging
purposes.

Send the command "primed=1" to prime the trigger, and receive a measure
of the time between receiving the priming command and detecting a sound.
If you want to use the sketch purely as a software-triggered pulse
generator, you can just repeatedly send this command without worrying
about the sound input or serial-port responses (you could even set
"audio_input_pin=-1" to disable analog reading). Priming causes a
pulse to be generated on the current "priming_output_pin".

Send "threshold=XXX" to change the detection threshold to numeric
value XXX. If the signal's absolute sample-to-sample difference
exceeds this value while the trigger is primed, a trigger pulse
will be generated on "trigger_output_pin".

Send "pulse_duration_msec=MMM" to change the duration of the pulses
on the "priming_output_pin" and "trigger_output_pin".

Send "reprime_interval_msec=MMM" if you want to use the sketch purely
as a hardware-triggered pulse generator (no further serial communication
necessary). The system automatically re-primes itself MMM msec after
each trigger, so you can think of this parameter as the refractory
period. Set back to 0 or to a negative value to disable automatic
re-priming.

Send "report_period_msec=MMM" to get repeated readings of the absolute
audio amplitude (helps in determining the correct threshold but may
interfere with your attempts to read the reply to "primed=1", so only
do it form the Arduino IDE's own Serial Monitor window). Set it to 0
to disable this debugging tool again before priming.

Send "debug_sample_timing=1" to get repeated readings of the number
of microseconds elapsed since priming, every nominal-1-seconds-worth
of samples after priming. Again, only do this from the Arduino IDE's
Serial Monitor window. Set it to 0 to disable this debugging tool
again before you perform any serious latency measurements.


Adjust the pin numbers in the following section according to the way
your hardware is going to be connected to your microcontroller:
`audio_input_pin`, as well as `priming_output_pin` (for software-
triggered pulses) and `trigger_output_pin` (for hardware-triggered
pulses) can all be reconfigured by sending serial-port commands---
for example, you could say `audio_input_pin=8`. However, every time
the microcontroller is turned off and on again they will revert to
the `DEFAULT_*` values you see here in the code.  You can set any
pin number (even `POWER_INDICATOR_LED`) to -1 to disable the
corresponding functionality.
*/                                                  // Location, in our trigger box v2:
const int   POWER_INDICATOR_LED           =    13;  // SPARE_LED_PINS[ 0 ], i.e. topmost LED in strip.  Change to `LED_BUILTIN` to use the onboard LED. 
const int   DEFAULT_AUDIO_INPUT_PIN       =    36;  // TRS_TIP_PINS[ 1 ], i.e. top audio jack, left channel (analogRead-capable)
const int   DEFAULT_PRIMING_OUTPUT_PIN    =     4;  // PARALLEL_PORT_OUTPUT_PINS[ 5 ], i.e. D5 and its corresponding LED
const int   DEFAULT_TRIGGER_OUTPUT_PIN    =     5;  // PARALLEL_PORT_OUTPUT_PINS[ 6 ], i.e. D6 and its corresponding LED


const float DEFAULT_THRESHOLD             =   800;  // initial value for `threshold`
const float DEFAULT_PULSE_DURATION_MSEC   =    20;  // initial value for `pulse_duration_msec`

const float DEFAULT_REPRIME_INTERVAL_MSEC =     0;  // If `reprime_interval_msec` is set >0, the sketch can act as a hardware-triggered pulse generator because it will re-prime the trigger automatically this many milliseconds after each trigger (so think of this as the refactory period)

#include "SerialInterface.h"   // needs Arduino IDE 1.8+ (and hence Teensyduino 1.48+) to support inclusion of a header direct from the sketch folder (as opposed to a subdirectory of %USERPROFILE%\Documents\Arduino\libraries)
// comment out this #include for the dirt-simple version (no configurable variables, no debugging, just prime whenever any line at all is received over the serial port)

const int   TRIGGER_SAMPLING_RATE_HZ      = 10000;  // Rate at which to sample the audio, in Hz (NB: make sure this divides into 1 million)
const int   BAUD                          = 38400;  // Serial port communication rate (probably ignored, for USB connections)

//////////////////////////////////////////////////////////////////////////////////////////////////////

bool gSampleReady;
IntervalTimer gSamplingTimer;
void SamplingCallback() { gSampleReady = true; }

float gPreviousReading;
float gThreshold;
int   gPrimed;
int   gPrimingOutputCountdown;
int   gTriggerOutputCountdown;
int   gAutoRePrimingCountdown;
unsigned long gPrimeTime;
float gOutputPulseDurationMsec;

int   gDebugSampleTiming;

float gRecentMaxAbsDiff;
int   gMaxReportCountdown;
float gReportPeriodMsec;
float gAutoRePrimeIntervalMsec;

int   gPrimingOutputPin;
int   gTriggerOutputPin;
int   gAudioInputPin;

void prime()
{
  gPrimeTime = micros();
  gPrimed = 1;
  if( gPrimingOutputPin >=  0 )
  {
    digitalWrite( gPrimingOutputPin, HIGH );
    gPrimingOutputCountdown = ( int )( 0.5 + TRIGGER_SAMPLING_RATE_HZ * gOutputPulseDurationMsec / 1000.0 );
  }
  gAutoRePrimingCountdown = 0;
}

void ready_pin( int pin, int mode, int initialValue )
{
  if( pin < 0 ) return;
  pinMode( pin, mode );
  if( mode == OUTPUT ) digitalWrite( pin, initialValue );
}

void setup()
{
  Serial.begin( BAUD );
  analogReadResolution( 16 );
  analogReadAveraging( 4 );
  analogReference( DEFAULT );

  gSampleReady = false;
  gSamplingTimer.begin( SamplingCallback, 1000000 / TRIGGER_SAMPLING_RATE_HZ );
  
  if( POWER_INDICATOR_LED >= 0 )
  { // Blink power LED for a couple of seconds to indicate that the unit has been restarted
    pinMode( POWER_INDICATOR_LED, OUTPUT );
    for( int repeat = 0; repeat < 10; repeat++ )
    {
      delay( 100 );
      digitalWrite( POWER_INDICATOR_LED, LOW );
      delay( 150 );
      digitalWrite( POWER_INDICATOR_LED, HIGH );
    }
  }

  gThreshold = DEFAULT_THRESHOLD;
  gPrimed = 0;
  gPrimingOutputCountdown = 0;
  gTriggerOutputCountdown = 0;
  gAutoRePrimingCountdown = 0; 
  gOutputPulseDurationMsec = DEFAULT_PULSE_DURATION_MSEC;
  gDebugSampleTiming = 0;
  gMaxReportCountdown = 0;
  gPreviousReading = 0.0;
  gRecentMaxAbsDiff = 0.0;
  gReportPeriodMsec = 0.0;
  gAutoRePrimeIntervalMsec = DEFAULT_REPRIME_INTERVAL_MSEC;

  gPrimingOutputPin = DEFAULT_PRIMING_OUTPUT_PIN;
  gTriggerOutputPin = DEFAULT_TRIGGER_OUTPUT_PIN;
  gAudioInputPin = DEFAULT_AUDIO_INPUT_PIN;
  ready_pin( gPrimingOutputPin, OUTPUT, LOW );
  ready_pin( gTriggerOutputPin, OUTPUT, LOW );
  ready_pin( gAudioInputPin,     INPUT,   0 );
}

void loop()
{
  
  if( gSampleReady )
  {
    gSampleReady = false;
    if( gAudioInputPin >= 0 )
    {
      float reading = ( float )analogRead( gAudioInputPin );
      float absDiff = fabs( reading - gPreviousReading );
      if( gPrimed )
      {
        int elapsedSamples = gPrimed - 1;
        if( absDiff > gThreshold )
        {
          if( gTriggerOutputPin  >= 0 )
          {
            digitalWrite( gTriggerOutputPin,  HIGH  );
            gTriggerOutputCountdown = ( int )( 0.5 + TRIGGER_SAMPLING_RATE_HZ * gOutputPulseDurationMsec / 1000.0 );
          }
          Serial.print( "{\"internal\": " );
          Serial.print( 1000.0f * ( float )elapsedSamples / ( float )TRIGGER_SAMPLING_RATE_HZ );
          Serial.println( "}" );
          Serial.flush();
          gPrimed = 0;
        }
        else
        {
          // report sample timing every second, if debug mode requested
        	if( gDebugSampleTiming && elapsedSamples && elapsedSamples % TRIGGER_SAMPLING_RATE_HZ == 0 )
        	{
            // Every 1 second's worth of samples after priming, print the elapsed microseconds since priming.
        	  // When viewed in the Serial Monitor window of the Arduino IDE, this acts as a diagnostic to ensure
            // that the Teensy sampling loop is meeting its own realtime deadlines.
            unsigned long delta = micros() - gPrimeTime; // casting delta to unsigned long provides protection against wraparound errors, (unless you actually leave it primed-but-untriggered for longer than 71 mins 35 seconds)
            Serial.print( "{\"elapsed_micros\": " ); 
            Serial.print( delta );
            Serial.println( "}" );
            Serial.flush();
        	}
  
          // keep counting
          gPrimed++;
        }
      }
      
      // report recent max. abs. diff. readings (to sanity-check signal connection and help setting threshold)
      if( gMaxReportCountdown < 0 )
      {
        gRecentMaxAbsDiff = 0.0;
        gMaxReportCountdown = ( int )( 0.5 + TRIGGER_SAMPLING_RATE_HZ * gReportPeriodMsec / 1000.0 );
      }
      if( gMaxReportCountdown )
      {
        gRecentMaxAbsDiff = max( gRecentMaxAbsDiff, absDiff );
        if( !--gMaxReportCountdown )
        {
          Serial.print( "{" );
          Serial.print( "\"max_abs_diff\": " ); Serial.print( gRecentMaxAbsDiff );
          //Serial.println( ", " );
          Serial.println( "}" );
          Serial.flush();
          gMaxReportCountdown = -1;
        }
      }
  
      gPreviousReading = reading;
    }
    else // no audio input
    {
      gPrimed = 0; // there's no point in staying primed if there's no audio input
    }
    
    // turn off the lights if they've been on long enough
    if( gPrimingOutputCountdown && !--gPrimingOutputCountdown ) digitalWrite( gPrimingOutputPin,  LOW  );
    if( gTriggerOutputCountdown && !--gTriggerOutputCountdown ) digitalWrite( gTriggerOutputPin,  LOW  );
    
    if( gAutoRePrimingCountdown && !--gAutoRePrimingCountdown && !gPrimed ) prime();
    if( !gPrimed && gAutoRePrimeIntervalMsec > 0.0 && !gAutoRePrimingCountdown ) gAutoRePrimingCountdown = ( int )( 0.5 + TRIGGER_SAMPLING_RATE_HZ * gAutoRePrimeIntervalMsec / 1000.0 );
  } // end sample
  
#ifdef __SerialInterface_H__  // if SerialInterface.h has been included
  // Tests with Teensy 3.1 indicate that this section (checking/setting the variables) takes 
  // about 1-2 microseconds per loop if no serial input is happening, or 30-50 microseconds
  // on loop iterations when there's a single variable command to process.  Either way, it
  // shouldn't affect performance.
  if( StartCommands() )
  {
	  if( ProcessVariableCommand( "primed", gPrimed ) && gPrimed ) prime();
	  ProcessVariableCommand( "threshold", gThreshold );
	  ProcessVariableCommand( "pulse_duration_msec", gOutputPulseDurationMsec );
	  ProcessVariableCommand( "reprime_interval_msec", gAutoRePrimeIntervalMsec );
	  if( ProcessVariableCommand( "priming_output_pin", gPrimingOutputPin ) ) ready_pin( gPrimingOutputPin, OUTPUT, LOW );
	  if( ProcessVariableCommand( "trigger_output_pin", gTriggerOutputPin ) ) ready_pin( gTriggerOutputPin, OUTPUT, LOW );
	  if( ProcessVariableCommand( "audio_input_pin",    gAudioInputPin    ) ) ready_pin( gAudioInputPin,     INPUT,   0 );
	  if( ProcessVariableCommand( "report_period_msec", gReportPeriodMsec ) ) gMaxReportCountdown = -1;
	  ProcessVariableCommand( "debug_sample_timing", gDebugSampleTiming );
	  EndCommands();
  }
  
#else // dirt-simple version: prime when you receive any line at all over the serial port
  if( Serial.read() == '\n' ) prime();
#endif

}
