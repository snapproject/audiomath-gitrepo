# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
import matplotlib.patches

def FormatWithUnits( value, context=None, units='', fmt='%+g', stripZeroSign=True, appendUnits=True ):
	"""
	Let's say your `value` is expressed in uninflected `units` (`'V'`
	for Volts or `'s'` for seconds) and you want a string representation
	of that value in the most convenient form of those units (nano-,
	micro-, or milli- units, or just plain units).  This function formats
	such a string, appending the inflected units string itself unless you
	explicitly say `appendUnits=False`.  The decision about whether to go
	nano, micro, or milli is taken according to the magnitude of `value`
	itself, unless you supply a list of values in `context`:  in the
	latter case, the maximum absolute value in `context` is used to set the
	scale.  For example, you might want to call `FormatWithUnits` once for
	each tick label on an axis, but with `context` equal to the full set of
	tick values each time so that they are all scaled the same.
	"""
	if context == None: context = [ value ]
	extreme = max( abs( x ) for x in context )
	if units == None: units = ''
	if units == '':           factor = 1e0; prefix = ''
	elif extreme <=  2000e-9: factor = 1e9; prefix = 'n'
	elif extreme <=  2000e-6: factor = 1e6; prefix = u'\u00b5'   # up to +/- 2 milliVolts, use microVolts
	elif extreme <=  2000e-3: factor = 1e3; prefix = 'm'         # up to +/- 2 Volts, use milliVolts
	else:                     factor = 1e0; prefix = ''
	if callable( fmt ): s = fmt( value * factor )
	else: s = fmt % ( value * factor )
	if stripZeroSign and value == 0.0 and s.startswith( ( '-', '+' ) ): s = s[ 1: ]
	if appendUnits: s += prefix + units
	return s

class StickySpanSelector( object ): # definition adapted from matplotlib.widgets.SpanSelector in matplotlib version 0.99.0
	"""
	A novel matplotlib widget loosely based on `matplotlib.widgets.SpanSelector`
	The main difference is that the `StickySpanSelector` is visible all the time.
	Its edges can be adjusted by clicking near them and dragging, or by using
	the arrow keys (press the enter key to toggle between edges, and between
	StickySpanSelectors if there's more than one on the axes).
	"""
	def __init__( self, ax, onselect=None, initial=None, direction='horizontal', fmt='%+g', units='', minspan=None, granularity=None, useblit=False, **props ):
		"""
		Args:
			ax (Axes)
				is the `matplotlib.pyplot.axes` instance on which the widget will
				be drawn.
		
			onselect (callable)
				the callback called whenever the span changes (the callback
				should take the upper and lower values of the span as two separate
				arguments).
		
			`initial` (None, tuple, list)
				specifies the initial data range over which to draw the selector
				(`None` means the selector does not appear until the user clicks
				for the first time).
				
			`direction` (str)
				may be 'horizontal' (the default) or 'vertical'.
		
			`fmt` (str)
				is passed to `FormatWithUnits`, along with `units`, to pretty-print
				the numeric values of the span limits at the top of the selector.
		
			`units` (str)
				is passed to `FormatWithUnits`, along with `fmt`, to pretty-print
				the numeric values of the span limits at the top of the selector.
		
			`minspan` (float)
				is a scalar value specifying the minimum allowable width of the
				selector.
		
			`granularity` (float)
				specifies the base to which span endpoints are rounded.
		
			`useblit` (bool)
				can remain equal to `False` (its implementation was copied from
				`matplotlib.widgets.SpanSelector	 but I'm not sure what it is
				for - it hasn't ever proved necessary).
		
		Additional keyword arguments may specify color, etc and are applied to both
		the `matplotlib.patches.Rectangle` that forms the main body of the rendered
		selector, and the text objects that render the numerical limit values. To
		restrict to one or the other, prepend rect_ or text_ to the keyword (e.g.
		`text_color='#000000'`). `text_y`  and `text_verticalalignment` are useful
		for changing the position of the text labels so that the labels from multiple
		selectors do not clash. `text_visible=False` is an easy way to get rid of the
		labels.
		"""
		if direction not in ['horizontal', 'vertical']: raise ValueError( 'Must choose "horizontal" or "vertical" for direction' )
		self.direction = direction
		self.axes = None
		self.canvas = None
		self.visible = True
		self.units = units
		self.fmt = fmt
		self.granularity = granularity
		self._lastMoved = 0

		self.rect = None
		self.background = None
		self._pressV = None
		self._cids = []
		self._keypressResponders = []
		self._updateResponders = []

		textprops, rectprops = {}, {}
		color = props.pop( 'color', ( 1, 0, 0 ) )
		for k, v in dict( props ).items():
			if   k.startswith( 'text_' ): textprops[ k[ 5: ] ] = props.pop( k )
			elif k.startswith( 'rect_' ): rectprops[ k[ 5: ] ] = props.pop( k )
			else: rectprops[ k ] = props.pop( k )
		
		self.rectprops = dict( linewidth=2, edgecolor=color, facecolor=color, alpha=0.4 )
		self.rectprops.update( rectprops )
		self.textprops = dict( color=( 1, 1, 1 ), backgroundcolor=color, x=1.0, y=1.0, verticalalignment='bottom', horizontalalignment='left' )
		self.textprops.update( textprops )
		
		self.onselect = onselect
		self.useblit = useblit
		self.minspan = minspan

		# Needed when dragging out of axes
		self.buttonIsDown = False
		self.prev = ( 0, 0 )

		self.NewAxes(ax)
		self.SetSpan( initial )
	
	def IsAlive( self ):
		return self.rect in self.rect.axes.patches
			
	def GetSiblings( self ):
		"""
		Return a list of pointers to other `StickySpanSelector` instances
		that go in the same direction on the same axes as `self`.
		"""
		attr = self.direction + 'StickySpanSelectors'
		if not hasattr( self.axes, attr ): setattr( self.axes, attr, [] )
		return getattr( self.axes, attr )
				
	def NewAxes( self, ax ):
		"""
		Helper function for creating the actual `matplotlib` artists and
		performing one-time setup of the mouse and keyboard callbacks.
		Called during construction of the instance.
		"""
		self.axes = ax
		self.GetSiblings().append( self )
		if self.canvas is not ax.figure.canvas:
			for cid in self._cids:
				self.canvas.mpl_disconnect( cid )
			self.canvas = ax.figure.canvas
			self._cids.append( self.canvas.mpl_connect( 'motion_notify_event', self.OnMove ) )
			self._cids.append( self.canvas.mpl_connect( 'button_press_event', self.OnMousePress ) )
			self._cids.append( self.canvas.mpl_connect( 'button_release_event', self.OnMouseRelease ) )
			self._cids.append( self.canvas.mpl_connect( 'draw_event', self.UpdateBackground ) )
			self._cids.append( self.canvas.mpl_connect( 'key_press_event', self.OnKeyPress ) )
		if self.direction == 'horizontal':
			trans = self.axes.get_xaxis_transform()
			self.rect = matplotlib.patches.Rectangle( ( 0, -0.01 ), 0, 1.02, transform=trans, visible=False, **self.rectprops )
		else:
			trans = self.axes.get_yaxis_transform()
			self.rect = matplotlib.patches.Rectangle( ( -0.01, 0 ), 1.02, 0, transform=trans, visible=False, **self.rectprops )
			
		if not self.useblit: self.axes.add_patch( self.rect )
		self.mintext = self.axes.text( s='min', transform=trans, **self.textprops )
		self.maxtext = self.axes.text( s='max', transform=trans, **self.textprops )
		self.UpdateText()
	
	def OnKeyPress( self, event ):
		"""
		`matplotlib` key-press callback, registered during `.NewAxes()`
		using the canvas's `.mpl_connect()` method.
		"""
		if self.axes is not self.axes.figure.gca(): return True
		if self is not self.GetSiblings()[ -1 ]: return True
		if event.key == None: return True
		coords = list( self.GetSpan() )
		gran = self.granularity
		mods = str( event.key ).split( '+' )
		key = mods.pop( -1 )
		if gran == None: minv, maxv = self.GetSpan(); gran = max( abs( maxv ), abs( minv ) ) / 100.0
		if 'alt' in mods: gran /= 5.0
		if 'shift' in mods: gran *= 10.0
		
		if   key in [ 'left'  ] and self.direction == 'horizontal': coords[ self._lastMoved ] -= gran
		elif key in [ 'right' ] and self.direction == 'horizontal': coords[ self._lastMoved ] += gran
		elif key in [ 'down'  ] and self.direction == 'vertical':   coords[ self._lastMoved ] -= gran
		elif key in [ 'up'    ] and self.direction == 'vertical':   coords[ self._lastMoved ] += gran
		elif key in [ 'enter' ]:
			attr = self.direction + 'StickySpanSelectorKeyPressHandled'
			previouslyHandled = getattr( event, attr, False )
			setattr( event, attr, True )
			if not previouslyHandled:
				self._lastMoved += 1
				if self._lastMoved > 1:
					self._lastMoved = 0
					sibs = self.GetSiblings()
					sibs.remove( self )
					sibs.insert( 0, self )
					sibs[ -1 ]._lastMoved = 0
			return True
		else:
			for func in self._keypressResponders:
				if not func( self, key, mods ): return False
			return True
		if coords[ 1 ] < coords[ 0 ]: self._lastMoved = 1 - self._lastMoved
		self.SetSpan( coords )
		return False
	
	def AddKeyPressResponder( self, r ):
		if r: self._keypressResponders.append( r )
		return r
	
	def AddUpdateResponder( self, r ):
		if r: self._updateResponders.append( r )
		return r
	
	def GetSpan( self ):
		"""
		Return the current span, as a two-element tuple.
		"""
		if self.direction == 'horizontal':
			start = self.rect.get_x()
			extent = self.rect.get_width()
		else:
			start = self.rect.get_y()
			extent = self.rect.get_height()
		return ( start, start + extent )
		
	def SetSpan( self, span, trigger_callback=True, rerender=True ):
		"""
		Set the `span` as a two-element tuple or list. By default,
		call `self.onselect()` (but do not do so if `trigger_callback`
		is `False`---this mode will not normally be needed but is used
		internally in `.OnMove()` )
		"""
		if span == None:
			self.rect.set_visible( False )
			self.mintext.set_visible( False )
			self.maxtext.set_visible( False )
		else:
			self.rect.set_visible( self.visible )
			self.mintext.set_visible( self.visible )
			self.maxtext.set_visible( self.visible )
			vmin, vmax = min( span ), max( span )
			if self.direction == 'horizontal':
				self.rect.set_x( vmin )
				self.rect.set_width( vmax - vmin )
			else:
				self.rect.set_y( vmin )
				self.rect.set_height( vmax - vmin )
			if trigger_callback and self.onselect:
				self.onselect( vmin, vmax )				
			self.UpdateText()
		if rerender:
			self.Update()
			self.canvas.draw()

	def UpdateBackground(self, event):
		"""
		Force an update of the background (relevant for `useblit=True`
		mode only).
		"""
		if self.useblit:
			self.background = self.canvas.copy_from_bbox( self.axes.bbox )

	def EventIsRelevant( self, event, v ):
		"""
		Return `False` if event should be ignored.
		"""
		if event.inaxes != self.axes or not self.visible or event.button != 1: return False
		nearest = None
		dmin = None
		for sib in self.GetSiblings():
			d = min( abs( lim - v ) for lim in sib.GetSpan() )
			if dmin == None or d < dmin: dmin = d; nearest = sib
		return ( self is nearest )

	def Focus( self ):
		"""
		Helper method: ensure that this selector's host axes object has
		keyboard focus.
		"""
		sibs = self.GetSiblings()
		sibs.remove( self )
		sibs.append( self )
		self.axes.figure.sca( self.axes )
		try: self.axes.figure.canvas._tkcanvas.focus_set()
		except: pass
		
	def OnMousePress( self, event ):
		"""
		`matplotlib` mouse-button-press callback, registered during
		`.NewAxes()` using the canvas's `.mpl_connect()` method.
		"""
		if self.direction == 'horizontal': v = event.xdata
		else:                              v = event.ydata
			
		if not self.EventIsRelevant( event, v ): return
		self.buttonIsDown = True
		self.rect.set_visible( self.visible )
		
		span = self.GetSpan()
		if abs( v - min( span ) ) < abs( v - max( span ) ): self._pressV = max( span ); self._lastMoved = 0
		else:                                               self._pressV = min( span ); self._lastMoved = 1
		self.Focus()
		return self.OnMove( event ) # if you press and hold without moving, the line still responds
		
	def OnMouseRelease( self, event ):
		"""
		`matplotlib` mouse-button-release callback, registered during
		.`NewAxes()` using the canvas's `.mpl_connect()` method.
		"""
		
		vmin = self._pressV
		if self.direction == 'horizontal': vmax = event.xdata or self.prev[ 0 ]
		else:                              vmax = event.ydata or self.prev[ 1 ]

		if vmin is None or vmax is None or not self.buttonIsDown: return
		self.buttonIsDown = False

		if vmin > vmax: vmin, vmax = vmax, vmin
		vmin = self.Round( vmin )
		vmax = self.Round( vmax )
		span = vmax - vmin
		if self.minspan is not None and span < self.minspan:
			print( 'TODO' ) # TODO: it's not enough to just return here: need to actually move the boundaries
		self.SetSpan( ( vmin, vmax ) )
		self._pressV = None
		return False

	def OnMove( self, event ):
		"""
		`matplotlib` mouse-moved callback, registered during
		.`NewAxes()` using the canvas's `.mpl_connect()` method.
		Calls `.SetSpan()` and hence `.Update()`.		
		"""
		x, y = event.xdata, event.ydata
		if self.direction == 'horizontal': v = x
		else:                              v = y
		if self._pressV is None or v is None or not self.buttonIsDown: return
		self.prev = x, y
		self.SetSpan( ( self.Round( v ), self.Round( self._pressV ) ), trigger_callback=False )
		return False

	def Update( self ):
		"""
		(Re-)render using newfangled blit or oldfangled draw depending
		on `.useblit`. Called automatically by `.SetSpan()`, which is in
		turn called during `.OnMove()`.
		"""
		if self.useblit:
			if self.background is not None: self.canvas.restore_region( self.background )
			self.axes.draw_artist( self.rect )
			self.canvas.blit( self.axes.bbox )
		else:
			self.canvas.draw_idle()
		for func in self._updateResponders: func( self )
		return False

	def UpdateText( self ):
		"""
		Separate `.Update()` function for the text label artists. Called
		automatically by `.SetSpan()` when appropriate.
		"""
		minv, maxv = self.GetSpan()
		context = self.GetAxisLimits()
		minvtext = FormatWithUnits( value=minv, context=context, units=self.units, fmt=self.fmt )
		maxvtext = FormatWithUnits( value=maxv, context=context, units=self.units, fmt=self.fmt )
		def combine( d, **k ): d = dict( d ); d.update( k ); return d
		if self.direction == 'horizontal':
			if minv == maxv:
				self.mintext.set( visible=False )
				self.maxtext.set( **combine( self.textprops, x=maxv, text=maxvtext, visible=self.visible, horizontalalignment='center' ) )
			else:
				self.mintext.set( **combine( self.textprops, x=minv, text=minvtext, visible=self.visible, horizontalalignment='right' ) )
				self.maxtext.set( **combine( self.textprops, x=maxv, text=maxvtext, visible=self.visible, horizontalalignment='left'  ) )
		else:
			if minv == maxv:
				self.mintext.set( visible=False )
				self.maxtext.set( **combine( self.textprops, y=maxv, text=maxvtext, visible=self.visible, verticalalignment='center' ) )
			else:
				self.mintext.set( **combine( self.textprops, y=minv, text=minvtext, visible=self.visible, verticalalignment='top'     ) )
				self.maxtext.set( **combine( self.textprops, y=maxv, text=maxvtext, visible=self.visible, verticalalignment='bottom'  ) )
			
	def GetAxisLimits( self ):
		"""
		Return, as a two-element sequence, the current axes' axis limits
		in the relevant direction.
		"""
		if self.direction == 'horizontal': return self.axes.get_xlim()
		else:                              return self.axes.get_ylim()
			
	def Round( self, value ):
		"""
		Helper method: if `self.granularity` has been set, return an
		accordingly rounded version of the input `value`.
		"""
		gran = self.granularity
		#lims = self.GetAxisLimits()
		#gran = max( abs( x ) for x in lims ) * self.granularity
		if not gran: return value
		return gran * round( value / gran )
