#!/usr/bin/env python3
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
"""
TODO:
	- works from IPython, but sound traces are
	  omitted from the figure if run non-interactively.
	
	- watch availability of font depending on OS (currently
	  works on macOS High Sierra).
"""
import os
import sys
import argparse

import audiomath as am

if __name__ == '__main__':
	parser = argparse.ArgumentParser( description=__doc__ )
	parser.add_argument( "-t", "--transparent", action='store_true', default=False, help='make background transparent' )
	parser.add_argument( "-s", "--save", "--save-as", metavar='FILENAME', action='store', default='', help='filename for saving (or format for default filename, if starts with `.`)' )
	args = parser.parse_args()

	plt = am.LoadPyplot()
	plt.figure( figsize=[ 5, 5 ] )
	s = am.TestSound( '12' )
	s.Plot()
	ax = plt.gca()
	fig = ax.figure
	ax.set( title='', xlabel='', ylabel='', xticks=[], yticks=[], xlim=[ 0, 0.95 ] )
	plt.subplots_adjust( 0, -0.3, 1.0, 1.3 )
	t = ax.text( 0.5, 0.5, 'one two', transform=ax.transAxes, ha='center', va='center', 
		fontname='Bitstream Vera Sans Mono', fontweight='bold', fontsize=80, 
	)
	if args.transparent:
		ax.set_axis_off()
		ax.patch.set_alpha( 0 )
		fig.patch.set_alpha( 0 )
		suffix = '-Transparent'
	else:
		suffix = '-WhiteBG'
	plt.draw()
	if args.save:
		if args.save.startswith( '.' ): args.save = 'AudiomathLogo-OneTwo' + suffix + args.save
		fig.savefig( args.save )
	am.Graphics.FinishFigure()
