.. default-role:: code

Accelerator
===========

* On Windows, install Visual Studio
* On macOS, install gcc, g++, git, etc with `xcode-select --install`
* On macOS or Windows, install CMake using the installer from https://cmake.org
* On Linux, `sudo apt install cmake git`

Then::

    ./accel-src/devel/build/go.cmd

Note the platform/architecture identifiers that the resulting dynamic library
name uses (obtained by `DefineFunction_TARGET_ARCHITECTURE.cmake`)
For a new platform like the M1, ensure that Python retrieves the same
identifier from `platform.machine().lower()` when it runs
`audiomath.accel.LoadLib()`.

Is it worth trying to make an M1+Intel universal binary? The advantage would be
in the ability to cross-compile to make both on either. Here is a cross-compiling
breadcrumb from a comment under https://stackoverflow.com/a/65385996 ::

    clang++ main.cpp -target arm64-apple-macos11 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX11.1.sdk


PortAudio
=========

::

    git clone --recursive https://github.com/PortAudio/portaudio
    cd portaudio
    ./configure
    make

Look for the dynamic library in `lib/.libs` (the one with the version number
in it; the one without is a symlink). Copy it to `python/audiomath/_wrap_portaudio/`,
renaming it according to the conventions of that directory using the appropriate
platform/architecture identifiers (same ones that CMake retrieved when making the
accelerator). Verify that `LoadLib()` in `python/audiomath/_wrap_portaudio/_dll_wrapper.py`
gets the right identifiers.


AVbin
=====

macOS prerequisites for AVbin
-----------------------------

* Install gcc, g++, git::

      xcode-select --install

* Install brew (see https://brew.sh ) in order to install yasm::

      /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

* Install yasm::

      brew install yasm 


Linux prerequisites for AVbin
-----------------------------

* Install yasm and libbz2::

      sudo apt install yasm libbz2-dev


Windows prerequisites for AVbin
-------------------------------

AVbin for Windows is built by cross-compiling from Linux. See the details in the
AVbin `README.md` file.


Build AVbin
-----------

::

    git clone --recursive git@bitbucket.org:snapproject/avbin-fork.git
    cd avbin-fork
    
Then, to support a processor that AVbin didn't support, like the M1, you'll
need to hack build.sh and extend the existing set of `.configure` and `.Makefile`
files with your own pair. It might be possible to hack the `macosx-universal`
sections to build for both (but note that the methodology for bundling universal
binaries may have changed as the definition of "universal" has shifted). Then
again, it might not be worth it---there's no downside to having the libs separate.

Build with::

    ./build.sh macosx-x86-64
    ./build.sh linux-x86-64

...or however you've named the platform in the new M1 files. Possibly you
should replace `x86-64` with `arm64`, if that is what
`DefineFunction_TARGET_ARCHITECTURE.cmake` said when you built the accelerator.

Look for the dynamic library in the appropriate platform subdirectory of `dist`.
Copy it to `python/audiomath/_wrap_avbin/`, renaming it according to the conventions
of that directory using the appropriate platform/architecture identifiers (same ones
that CMake retrieved when making the accelerator). Verify that `LoadLib()` in
`python/audiomath/_wrap_avbin/__init__.py` gets the right identifiers.

AVbin-for-M1 update 2022-02-07
------------------------------

Sometimes (unpredictably, but seemingly non-deterministically affected by the name
of the dylib being changed) it will crash on load.

Other times, the dynamic library builds correctly and can be loaded and used.
However, on arm64 (but not x86_64) it delivers corrupted results from the call to
the dynamic library function `avbin_stream_info()` in `AVbinSource.__init__()` in
`audiomath/_wrap_avbin/__init__.py`

    print(info.u.audio.channels)      # comes out correctly as 2
    print(info.u.audio.sample_bits)   # should be 16, comes out as (uint32)(-1)
    print(info.u.audio.sample_format) # should be 1, comes out as -1
    print(info.u.audio.sample_rate)   # comes out correctly as 44100

This results in "OSError: failed to interpret audio format details..."

Unfortunately, even if you manually correct `sample_bits` to 16 and ignore the
apparently-unused `sample_format`, some further corruption must be happening during
decoding because the audio content does not look right.

This errpr appears to not actually be M1-specific, but rather a feature of the
version/settings of whatever software happened to generate all the test files I used
(remember, this also used to happen with .ogg files produced by a certain older version
of ffmpeg...). The cause appears to be that the AVBin wrapper cannot deal with "planar"
(i.e. non-interleaved) sample formats (even though the underlying libav library does
appear to understand them).

Note that AVBin is a dead project, and even the underlying libav seems to be dead now
since 2019.

Conclusions: (1) maybe the m1 build of AVbin is good enough to include, if the platform-
specific naming could be re-wrangled to avoid the segfault; (2) it's time to move away
from AVBin anyway, probably in favour of audioread which appears to share the audiomath
philosophy (supporting 2.7 as well as 3.x, having minimal dependencies, being flexible
between multiple backends, hiding the gory details...).