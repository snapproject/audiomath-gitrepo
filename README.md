* Permanent home: https://pypi.org/project/audiomath
* Documentation:  https://audiomath.readthedocs.io
* Master code repository: https://bitbucket.org/snapproject/audiomath-gitrepo
