:<<@GOTO:EOF
:: Windows section (NB: do not use backticks in this section)
@echo off
: ################################################################

set "PREFIX=%1

set MODE=
hg id -intb        >NUL 2>NUL && set MODE=hg
git rev-parse HEAD >NUL 2>NUL && set MODE=git

set REVISION=
if %MODE%==hg (
	for /f "tokens=*" %%x in ('hg id -intb') do set "REVISION=%%x"
)
if %MODE%==git (
	for /f "tokens=*" %%x in ('git log -1 "--format=%%h %%ci_"') do set "FIRST=%%x"
	for /f "tokens=*" %%x in ('git describe --all --always --long "--dirty=+" "--broken=!"') do set "SECOND=%%x"
	set REVISION=%FIRST:_= %%SECOND%
)
if NOT "%REVISION%"=="" set "REVISION=%MODE% %REVISION%"

for /F "tokens=1,2 delims==" %%i in ('wmic os get LocalDateTime /VALUE 2^>NUL') do if '.%%i.'=='.LocalDateTime.' set DATETIMESTAMP=%%j
set "YYYYMMDD=%DATETIMESTAMP:~0,8%
set   "HHMMSS=%DATETIMESTAMP:~8,6%

echo #define %PREFIX%_REVISION "%REVISION%"
echo #define %PREFIX%_DATESTAMP "%YYYYMMDD%-%HHMMSS%"

: ###################################################################
@GOTO:EOF
(tr -d \\r|env EXECNAME="$0" bash -s - "$@")<<":EOF"
: ###################################################################

# posix (bash) section

PREFIX=$1

HGREVISION=`hg id -intb                   2>/dev/null`
GITREVISION=`git log -1 "--format=%h %ci" 2>/dev/null`
[ -z "$HGREVISION"  ] || REVISION="hg $HGREVISION"
[ -z "$GITREVISION" ] || REVISION="git $GITREVISION `git describe --always --all --long --dirty=+ --broken='!' 2>/dev/null`"

echo '#define '$PREFIX'_REVISION  "'$REVISION'"'
echo '#define '$PREFIX'_DATESTAMP "'`date "+%Y%m%d-%H%M%S"`'"'

: ###################################################################
:EOF
