#!/bin/bash

:<<@GOTO:EOF
:: Windows section
@echo off
: ################################################################
cls

setlocal
setlocal EnableDelayedExpansion

set "TARGET=Win64"
set "IDE="
if not "%~1"=="" set "TARGET=%~1"
if not "%~2"=="" set "IDE=%~2"

call "%~dp0VisualStudio" %TARGET% %IDE% || exit /b 1
set "GENERATOR=Visual Studio %VSVERSION%"
if "%VS_USE_CMAKE_ARCH_FLAG%"=="" (
	if not "%TARGET%"=="Win32" set "GENERATOR=%GENERATOR% %TARGET%"
) else (
	if "%TARGET%"=="Win32" set "VS_USE_CMAKE_ARCH_FLAG=%VS_USE_CMAKE_ARCH_FLAG% %TARGET%"
	if "%TARGET%"=="Win64" set "VS_USE_CMAKE_ARCH_FLAG=%VS_USE_CMAKE_ARCH_FLAG% x64"
)
set "JUNK=junk-VS%VSVERSION%-%TARGET%"
set "STARTDIR=%CD%
cd "%~dp0"
::echo."%GENERATOR%" && echo."%VSVERSION%"

mkdir "%JUNK%" >NUL 2>NUL

cmake "-H." "-B%JUNK%" -G "%GENERATOR%" %VS_USE_CMAKE_ARCH_FLAG%
set PLATFORM=
msbuild /p:Configuration=Release "%JUNK%\audiochunk.sln"
cd "%STARTDIR%
endlocal

pause

: ###################################################################
@GOTO:EOF
(tr -d \\r|env EXECNAME="$0" bash "$@")<<":EOF"
: ###################################################################

# posix (bash) section

export MACOSX_DEPLOYMENT_TARGET=10.9
which python3 >/dev/null 2>&1 && PYTHON=python3 || PYTHON=python
$PYTHON <<-ENDPYTHON || export MACOSX_DEPLOYMENT_TARGET=10.4
import sys, platform
if sys.platform.lower().startswith( 'darwin' ):
    version = [ int( x ) for x in platform.mac_ver()[ 0 ].split( '.' ) ]
    if version < [ 10, 9 ]: sys.exit( 1 )
ENDPYTHON
#echo MACOSX_DEPLOYMENT_TARGET=$MACOSX_DEPLOYMENT_TARGET
# Under 10.13 (High Sierra, 2017) with gcc --version = "Apple LLVM 9.1.0":
# - you get a libstd++ deprecation warning if you target anything earlier than 10.9 (Mavericks, 2013)
# - the absolute farthest back you can go is 10.4 (Tiger, 2005)
# Update: now we only go back to 10.9 by default, not 10.4, because we need the newer libc++,
#         not the deprecated libstdc++, for c++11 features like steady_clock

STARTDIR=$(pwd)
TARGETDIR=$(dirname "$EXECNAME")
JUNK=junk
cd "$TARGETDIR"
mkdir -p "$JUNK"

if [ "$1" = "" ]; then
  cmake -H. -B$JUNK -G "Unix Makefiles"
  cd "$JUNK"
  make all
  cd "$STARTDIR"
else
  cmake -H. -B$JUNK "$@"
fi

: ###################################################################
:EOF
