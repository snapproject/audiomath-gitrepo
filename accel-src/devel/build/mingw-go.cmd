@echo off
:: NB: mingw64 (or mingw-w64) refers to a 64-bit distro of MinGW, NOT the seemingly-canonical one from mingw.org, which is only 32-bit as at 20190709
::     (it seems reasonably clear that you can't build a 64-bit target with the 32-bit toolchain, so mingw.org is out).
:: 
::     I got it in July 2019 from https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe
::     Then, during installation:
:: 
::     - changed location from `C:\Program Files....`  to `C:\MinGW64`
:: 
::     - chose options:
::         - Architecture: x86_64
::         - Threads:      win32
::         - Exception:    sjlj    ...because according to https://stackoverflow.com/a/19791995/  only this variant supports dual (32-bit and 64-bit) targetting
:: 
::     Finally, I ensured `C:\MinGW64\mingw64\bin`  was on the Windows path
::
::
:: TODO: How should we dictate whether we're building the 32-bit or 64-bit target, in this script?
::       
::       [1]: https://sourceforge.net/p/mingw-w64/wiki2/GeneralUsageInstructions/      
::       [2]: https://stackoverflow.com/questions/5805874/
::       [3]: https://stackoverflow.com/a/19791995/
::       

set "TARGET=Win64"
if not "%~1"=="" set "TARGET=%~1"
set FLAGS=
if "%TARGET%"=="Win32" set "FLAGS=-DCMAKE_CXX_FLAGS=-m32 -DCMAKE_C_FLAGS=-m32"

set "HERE=%~dp0
SET "HERE=%HERE:~0,-1%
set "JUNK=%HERE%\junk-mingw64-%TARGET%
if not exist "%JUNK%" mkdir "%JUNK%"

cmake "-H%HERE%" "-B%JUNK%" -G "MinGW Makefiles" %FLAGS%

mingw32-make -C "%JUNK%"
:: NB: it's called mingw32-make even if it's a 64-bit binary from mingw-w64 and is making 64-bit binaries
