# Downloaded 2014-08-26 from https://lost-contact.mit.edu/afs/cs.wisc.edu/p/graphics/Tools/CMake/Modules/CMakeDetermineSystem.cmake
# Around the web, CMAKE_SYSTEM_NAME seems to be assumed to be filled in automatically.
# However, my experience on Win7 64, targeting Windows i386, with cmake 2.8.12.2, has been
# that CMAKE_SYSTEM_NAME is blank unless you explicitly manually INCLUDE() a copy of this
# file --JH
# 
# This module is used by the Makefile generator to determine the following variables:
# CMAKE_SYSTEM_NAME - on unix this is uname -s, for windows it is Windows
# CMAKE_SYSTEM_VERSION - on unix this is uname -r, for windows it is empty
# CMAKE_SYSTEM - ${CMAKE_SYSTEM}-${CMAKE_SYSTEM_VERSION}, for windows: ${CMAKE_SYSTEM}
#
#  Expected uname -s output:
#
# AIX                           AIX  
# BSD/OS                        BSD/OS  
# FreeBSD                       FreeBSD  
# HP-UX                         HP-UX  
# IRIX                          IRIX  
# Linux                         Linux  
# NetBSD                        NetBSD  
# OpenBSD                       OpenBSD  
# OFS/1 (Digital Unix)          OSF1  
# SCO OpenServer 5              SCO_SV  
# SCO UnixWare 7                UnixWare  
# SCO UnixWare (pre release 7)  UNIX_SV  
# SCO XENIX                     Xenix  
# Solaris                       SunOS  
# SunOS                         SunOS  
# Tru64                         Tru64  
# Ultrix                        ULTRIX  
# cygwin                        CYGWIN_NT-5.1
# MacOSX                        Darwin
  
IF(UNIX)
  FIND_PROGRAM(CMAKE_UNAME uname /bin /usr/bin /usr/local/bin )
  IF(CMAKE_UNAME)
	EXECUTE_PROCESS(COMMAND sh -c "uname -s" OUTPUT_VARIABLE CMAKE_SYSTEM_NAME    OUTPUT_STRIP_TRAILING_WHITESPACE)
	EXECUTE_PROCESS(COMMAND sh -c "uname -r" OUTPUT_VARIABLE CMAKE_SYSTEM_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
    SET(CMAKE_UNAME ${CMAKE_UNAME} CACHE INTERNAL "uname command")
  ENDIF(CMAKE_UNAME)
ELSE(UNIX)
  IF(WIN32)
    SET (CMAKE_SYSTEM_NAME "Windows")
    SET (CMAKE_SYSTEM_VERSION "")
  ENDIF(WIN32)
ENDIF(UNIX)

IF(NOT CMAKE_SYSTEM_NAME)
  SET(CMAKE_SYSTEM_NAME "UnknownOS")
ENDIF(NOT CMAKE_SYSTEM_NAME)

# fix for BSD/OS , remove the /
IF(CMAKE_SYSTEM_NAME MATCHES BSD.OS)
  SET(CMAKE_SYSTEM_NAME BSDOS)
ENDIF(CMAKE_SYSTEM_NAME MATCHES BSD.OS)

# fix for CYGWIN which has windows version in it 
IF(CMAKE_SYSTEM_NAME MATCHES CYGWIN)
  SET(CMAKE_SYSTEM_NAME CYGWIN)
ENDIF(CMAKE_SYSTEM_NAME MATCHES CYGWIN)

# set CMAKE_SYSTEM to the CMAKE_SYSTEM_NAME
SET(CMAKE_SYSTEM  ${CMAKE_SYSTEM_NAME})
# if there is a CMAKE_SYSTEM_VERSION then add a -${CMAKE_SYSTEM_VERSION}
IF(CMAKE_SYSTEM_VERSION)
  SET(CMAKE_SYSTEM ${CMAKE_SYSTEM}-${CMAKE_SYSTEM_VERSION})
ENDIF(CMAKE_SYSTEM_VERSION)


# configure variables set in this file for fast reload
CONFIGURE_FILE(${CMAKE_ROOT}/Modules/CMakeSystem.cmake.in 
               ${CMAKE_BINARY_DIR}/CMakeSystem.cmake IMMEDIATE)

