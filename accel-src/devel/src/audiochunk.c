/*
# $BEGIN_AUDIOMATH_LICENSE$
# 
# This file is part of the audiomath project, a Python package for
# recording, manipulating and playing sound files.
# 
# Copyright (c) 2008-2025 Jeremy Hill
# 
# audiomath is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# The audiomath distribution includes binaries from the third-party
# AVbin and PortAudio projects, released under their own licenses.
# See the respective copyright, licensing and disclaimer information
# for these projects in the subdirectories `audiomath/_wrap_avbin`
# and `audiomath/_wrap_portaudio` . It also includes a fork of the
# third-party Python package `pycaw`, released under its original
# license (see `audiomath/pycaw_fork.py`).
# 
# $END_AUDIOMATH_LICENSE$
*/

/*
	# gcc -dynamiclib -o chunk.dylib chunk.c
	import ctypes
	dll = ctypes.CDLL( 'chunk.dylib' )
	dll.TransferAudioChunk.restype = ctypes.c_double
	dll.TransferAudioChunk.argtypes = (
		ctypes.c_char_p,                     # outputBaseAddress
		ctypes.c_uint,                       # outputItemSize
		ctypes.c_uint,                       # outputNumberOfSamples
		ctypes.c_uint,                       # outputNumberOfChannels
		ctypes.c_uint,                       # outputSampleStride
		ctypes.c_uint,                       # outputChannelStride
		
		ctypes.c_char_p,                     # inputBaseAddress
		ctypes.c_uint,                       # inputItemSize
		ctypes.c_uint,                       # inputNumberOfSamples
		ctypes.c_uint,                       # inputNumberOfChannels
		ctypes.c_uint,                       # inputSampleStride
		ctypes.c_uint,                       # inputChannelStride
		
		ctypes.c_uint,                       # isLooped
		ctypes.c_double,                     # speed
		ctypes.c_double,                     # inputSampleOffset

		ctypes.c_double,                     # leftVolume
		ctypes.c_double,                     # rightVolume
		ctypes.c_uint,                       # numberOfLevels
		ctypes.POINTER( ctypes.c_double ),   # levels
		
		ctypes.c_uint,                       # isFirstInChain
	)	
	p.Play( _accel=dll )
	
	result = transfer (
		frame.__array_interface__[ 'data' ][ 0 ],
		frame.dtype.itemsize,
		frame.shape[ 0 ],
		frame.shape[ 1 ],
		frame.strides[ 0 ],
		frame.strides[ 1 ],
		
		source.__array_interface__[ 'data' ][ 0 ],
		source.dtype.itemsize,
		source.shape[ 0 ],
		source.shape[ 1 ],
		source.strides[ 0 ],
		source.strides[ 1 ],
		
		self._loop,
		speed,
		self._nextSample,
		
		leftVolume  * self._volume,
		rightVolume * self._volume,
		len( levels ),
		levels,
	)
				
		
	# Bypass and fall back to pure-Python implementation if it's a Synth
*/

double
TransferAudioChunk(	
	char *       outputBaseAddress,
	unsigned int outputItemSize,
	unsigned int outputNumberOfSamples,
	unsigned int outputNumberOfChannels,
	unsigned int outputSampleStride,
	unsigned int outputChannelStride,

	char *       inputBaseAddress,
	unsigned int inputItemSize,
	unsigned int inputNumberOfSamples,
	unsigned int inputNumberOfChannels,
	unsigned int inputSampleStride,
	unsigned int inputChannelStride,

	int          isLooped,
	double       speed,
	double       inputSampleOffset,
	
	double       leftVolume,
	double       rightVolume,
	unsigned int numberOfLevels,
	double *     levels,
	
	unsigned int isFirstInChain
)
{
	unsigned int outputSampleIndex;
	char * outputSampleAddress;
	unsigned int outputChannelIndex;
	char * outputValueAddress;
	
	if( !outputBaseAddress ) return -1.0;
	// outputItemSize dealt with later: return -2.0;
	if( !outputNumberOfSamples ) return -3.0;
	if( !outputNumberOfChannels ) return -4.0;
	if( !outputSampleStride ) return -5.0;
	if( !outputChannelStride ) return -6.0;
	if( !inputBaseAddress ) return -7.0;
	// inputItemsize  dealt with later: return -8.0;
	if( !inputNumberOfSamples ) return -9.0;
	if( !inputNumberOfChannels ) return -10.0;
	if( !inputSampleStride ) return -11.0;
	// inputChannelStride can't go wrong (even if 0) return -12.0;
	// isLooped can't go wrong: return -13.0;
	// speed can't go wrong: return -14.0;
	// inputSampleOffset can't go wrong: return -15.0;
	// leftVolume can't go wrong: return -16.0;
	// rightVolume can't go wrong: return -17.0;
	// numberOfLevels can't go wrong: return -18.0;
	if( !levels && numberOfLevels > 0 ) return -19.0;
	// isFirstInChain can't go wrong: return -20.0;
	
	for(
		outputSampleIndex = 0, outputSampleAddress = outputBaseAddress;
		outputSampleIndex < outputNumberOfSamples;
		outputSampleIndex++,                       outputSampleAddress += outputSampleStride
	)
	{
		double value = 0.0;
		char * inputSampleAddress = 0;
		int inputSampleIndex = ( int )( inputSampleOffset + speed * outputSampleIndex );
		if( isLooped ) inputSampleIndex %= inputNumberOfSamples; 
		if( 0 <= inputSampleIndex && inputSampleIndex < ( int )inputNumberOfSamples )
			inputSampleAddress = inputBaseAddress + inputSampleStride * inputSampleIndex;
		for(
			outputChannelIndex = 0, outputValueAddress = outputSampleAddress;
			outputChannelIndex < outputNumberOfChannels;
			outputChannelIndex++,                       outputValueAddress += outputChannelStride
		)
		{
			if( inputSampleAddress )
			{
				char * inputValueAddress = inputSampleAddress + inputChannelStride * ( outputChannelIndex % inputNumberOfChannels );
				if(      inputItemSize == 4 ) value = ( double )*( float  * )inputValueAddress;
				else if( inputItemSize == 8 ) value = ( double )*( double * )inputValueAddress;
				else return -8.0;
				if( levels && numberOfLevels ) value *= levels[ outputChannelIndex % numberOfLevels ];
				value *= ( outputChannelIndex % 2 ) ? rightVolume : leftVolume;
			}
			if( isFirstInChain )
			{
				if(      outputItemSize == 4 ) *( float  * )outputValueAddress = ( float  )value;
				else if( outputItemSize == 8 ) *( double * )outputValueAddress = ( double )value;
				else return -2.0;
			}
			else
			{
				if(      outputItemSize == 4 ) *( float  * )outputValueAddress += ( float  )value;
				else if( outputItemSize == 8 ) *( double * )outputValueAddress += ( double )value;
				else return -2.0;
			}
		}	
	}
	return inputSampleOffset + speed * outputNumberOfSamples;
}
